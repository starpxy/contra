# README #

This work is an AI agent implementation for course Agent-Oriented Software 2020 assignment by Xingyu Pan. The project used game template from course Game Development 2020 by professor Abaraham Campbell. 

### How to run? ###
* Double click the contra.jar file to run
* Or you can make a shortcut for the jar file and click the short cut to run

### Play instruction ###

* Use 'AWSD' to move, Space/'k' to jump and 'J' to fire
* You can pause the game use 'P'

### Customize your map###
* Create json map in res/maps

### Game Implementation ###
* The game engine is a template from course Game Development 2020 by professor Abaraham Campbell. 
* Physics and game logic is implemented by Xingyu

### Art work, Music & Sound Effects###

* All of the art work in res/SpriteSheets were download from 
[https://didigameboy.itch.io/jambo-jungle-free-sprites-asset-pack](https://didigameboy.itch.io/jambo-jungle-free-sprites-asset-pack)
* Other art work/UI sprites are made by Xingyu Pan
* Music and sound effects are download from the internet. Please [contact me](mailto:xingyu.pan@ucdconnect.ie) if the work involves any infringement.