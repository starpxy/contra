import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.starpxy.controller.Controller;
import com.starpxy.model.Model;
import com.starpxy.music.MusicPlayer;
import com.starpxy.util.UnitTests;
import com.starpxy.util.Util;
import com.starpxy.view.DrawPanel;
import com.starpxy.view.SettingPanel;
import com.starpxy.view.StartViewer;
import com.starpxy.view.Viewer;

/*
 * Created by Abraham Campbell on 15/01/2020.
 *   Copyright (c) 2020  Abraham Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
   
   (MIT LICENSE ) e.g do what you want with this :-) 
 */

public class MainWindow {
	private static JFrame frame = new JFrame("Star's Contra"); // Change to the name of your game
	private static Model gameworld = new Model();
	private static Viewer canvas = new Viewer(gameworld);
	private KeyListener Controller = new Controller();
	private static int TargetFPS = 60;  
	public static boolean startGame = false;

	public MainWindow() {
		frame.setSize(1280, 720); // you can customise this later and adapt it to change on size.
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // If exit // you can modify with your way of quitting ,
																// just is a template.
		frame.setLayout(null);
		frame.add(canvas);
		canvas.setBounds(0, 0, 1280, 720);
		canvas.setBackground(new Color(255, 255, 255)); // white background replaced by Space background but if you
														// remove the background method this will draw a white screen
		canvas.setVisible(false); // this will become visible after you press the key.
		canvas.addKeyListener(Controller);
		StartViewer startViewer = new StartViewer();
		startViewer.setBounds(0, 0, 1280, 720);
		SettingPanel settingPanel = new SettingPanel(gameworld);
		settingPanel.setBounds(0, 0, 1280, 720);
		startViewer.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {
				int currentOption = startViewer.getCurrentOption();
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					currentOption += 1;
					currentOption = currentOption % 2;
				} else if (e.getKeyCode() == KeyEvent.VK_UP) {
					currentOption += 1;
					currentOption = currentOption % 2;
				} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					switch (currentOption) {
					case 0:// case 0: play the game
						startViewer.setVisible(false);
						canvas.setVisible(true);
						canvas.requestFocus();
						gameworld.setMode(currentOption);
						startGame = true;
						if (!gameworld.isMute()) {
							MusicPlayer.getInstance().stopStartBGM();
						}
						break;
					case 1:// case 1: go to setting
						startViewer.setVisible(false);
						settingPanel.setVisible(true);
						settingPanel.requestFocus();
						break;
					default:
						break;
					}
					return;
				}
				startViewer.setCurrentOption(currentOption);
				startViewer.repaint();
			}
		});
		settingPanel.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ESCAPE) {
					startViewer.setVisible(true);
					settingPanel.setVisible(false);
					startViewer.requestFocus();
				}
				else if (e.getKeyCode()==KeyEvent.VK_ENTER) {
					gameworld.setMute(!gameworld.isMute());
					settingPanel.repaint();
					if (gameworld.isMute()) {
						MusicPlayer.getInstance().stopStartBGM();
					}
				}
			}
		});
		frame.add(startViewer);
		frame.add(settingPanel);
		frame.setVisible(true);
		startViewer.requestFocus();
		// play start BGM
		if (!gameworld.isMute()) {
			MusicPlayer.getInstance().playStartBGM();
		}
	}

	public static void main(String[] args) {
		MainWindow hello = new MainWindow(); // sets up environment
		while (true) // not nice but remember we do just want to keep looping till the end. // this
						// could be replaced by a thread but again we want to keep things simple
		{
			// swing has timer class to help us time this but I'm writing my own, you can of
			// course use the timer, but I want to set FPS and display it

			int TimeBetweenFrames = 1000 / TargetFPS;
			long FrameCheck = System.currentTimeMillis() + (long) TimeBetweenFrames;
			// wait till next time step
			while (FrameCheck > System.currentTimeMillis()) {
			}

			if (startGame) {
				gameloop();
			}

			// UNIT test to see if framerate matches
			UnitTests.CheckFrameRate(System.currentTimeMillis(), FrameCheck, TargetFPS);

		}

	}

	// Basic Model-View-Controller pattern
	private static void gameloop() {
		// GAMELOOP

		// controller input will happen on its own thread
		// So no need to call it explicitly

		// model update
		gameworld.gamelogic();
		// view update

		canvas.updateview();

		// Both these calls could be setup as a thread but we want to simplify the game
		// logic for you.
		// score update
		frame.setTitle("Star's contra");
		Util.updateClock();

	}

}

/*
 * 
 * 
 * 
 * Hand shake agreement
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::,=+++
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::,,,,,,:::::,=+++????
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::,,,,,,,,,,,,,,:++++????+??
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * ::::::::::::::::::,:,:,,:,:,,,,,,,,,,,,,,,,,,,,++++++?+++++????
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,=++?+++++++++++??????
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * ::::,:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,~+++?+++?++?++++++++++?????
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::,:
 * ::,,,,,,,,,,,,,,,,,,,,,,,,,,,~+++++++++++++++????+++++++???????
 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::,:,,,,,
 * ,,,,,,,,,,,,,,,,,:===+=++++++++++++++++++++?+++????????????????
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::,,,,,,,,
 * ,,,,,,,,,,~=~~~======++++++++++++++++++++++++++????????????????
 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::,::::,,,,,,=~.,
 * ,,,,,,+===~~~~~~====++++++++++++++++++++++++++++???????????????
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::,:,,,,,~~.~??++~.,
 * ~~~~~======~=======++++++++++++++++++++++++++????????????????II
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::,:,,,,:=+++??=====~~~~
 * ~~====================+++++++++++++++++++++?????????????????III
 * :::::::::::::::::::::::::::::::::::::::::::::::::::,:,,,++~~~=+=~~~~~~==~~~::
 * ::~~==+++++++==++++++++++++++++++++++++++?????????????????IIIII
 * ::::::::::::::::::::::::::::::::::::::::::::::::,:,,,:++++==+??+=======~~~~=~
 * ::~~===++=+??++++++++++++++++++++++++?????????????????I?IIIIIII
 * ::::::::::::::::::::::::::::::::::::::::::::::::,,:+????+==??+++++?++====~~~~
 * ~:~~~++??+=+++++++++?++++++++++??+???????????????I?IIIIIIII7I77
 * ::::::::::::::::::::::::::::::::::::::::::::,,,,+???????++?+?+++???7?++======
 * ~~+=====??+???++++++??+?+++???????????????????IIIIIIIIIIIIIII77
 * :::::::::::::::::::::::::::::::::::::::,,,,,,=??????IIII7???+?+II$Z77??+++?+=
 * +++++=~==?++?+?++?????????????III?II?IIIIIIIIIIIIIIIIIIIIIIIIII
 * ::::::::::::::::::::::::::::::,,,,,,~=======++++???III7$???+++++Z77ZDZI?????I
 * ?777I+~~+=7+?II??????????????IIIIIIIIIIIIIIIIIIIIII??=:,,,,,,,,
 * ::::::::,:,:,,,,,,,:::~==+=++++++++++++=+=+++++++???I7$7I?+~~~I$I??++??
 * I78DDDO$7?++==~I+7I7IIIIIIIIIIIIIIIIII777I?=:,,,,,,,,,,,,,,,,,,,,,,,,
 * ++=++=++++++++++++++?+????+??????????+===+++++????I7$$ZZ$I+=~$7I???++++++===~
 * ~==7??++==7II?~,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * +++++++++++++?+++?++????????????IIIII?I+??I???????I7$ZOOZ7+=~7II?+++?II?I?+++
 * =+=~~~7?++:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * +?+++++????????????????I?I??I??IIIIIIII???II7II??I77$ZO8ZZ?~~7I?+==++?O7II??+
 * ??+=====.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * ?????????????III?II?????I?????IIIII???????II777IIII7$ZOO7?+~+7I?+=~~+???
 * 7NNN7II?+=+=++,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * ????????????IIIIIIIIII?IIIIIIIIIIII????II?III7I7777$ZZOO7++=$77I???==+++????
 * 7ZDN87I??=~,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * IIII?II??IIIIIIIIIIIIIIIIIIIIIIIIIII???+??II7777II7$$OZZI?+$$$$77IIII????????
 * ?++=+.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII?+++?IIIII7777$$$$$$7$$$$7IIII7I$IIIIII?
 * ??I+=,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII???????IIIIII77I7777$7$$$II????I??
 * I7Z87IIII?=,,,,,,,,,,,:,,::,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * 777777777777777777777I7I777777777~,,,,,,,+77IIIIIIIIIII7II7$$$Z$?I????III???
 * II?,,,,,,,,,,::,::::::::,,:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * 777777777777$77777777777+::::::::::::::,,,,,,,=7IIIII78ZI?II78$7++D7?7O777II?
 * ?:,,,:,,,::::::::::::::,:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * $$$$$$$$$$$$$77=:,:::::::::::::::::::::::::::,,7II$,,8ZZI++$8ZZ?+=ZI==IIII,+7
 * :,,,,:::::::::::::::::,:::,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * $$$I~::::::::::::::::::::::::::::::::::::::::::II+,,,OOO7?$DOZII$I$I7=77?,,,,
 * ,,:::::::::::::::::::::,,,:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * ::::::::::::::::::::::::::::::::::::::::::::::::::::::+ZZ?,$ZZ$77ZZ$?,,,,,:::
 * :::::::::::::::::::::::,::::,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::I$::::::::::::::
 * :::::::::::::::::::::::::::::,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * ::::::::::::::::::::::::::::::,,,:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * ::::::::::::::::::::::::::::::::::::,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * ::::::::::::::::::::::::::::::::::::,,,,,,,,,,,,,,,,,,,,,,,,,,,
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * ::::::::::::::::::::::::::::::::::::::,,,,,,,,,,,,,,,,,,,,,,,,,
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::,,,,,,,,,,,,,,,,,,,,,,
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::,,,,,,,,,,,,,,,,,,,,,,
 * GlassGiant.com
 * 
 * 
 */
