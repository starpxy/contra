package com.starpxy.agents;
/**
 * @author starpxy
 * 
 * This is a basic decision-making agent.
 * 
 */
import com.starpxy.agents.base.Agent;
import com.starpxy.gameobjects.EnemyGameObject;
import com.starpxy.gameobjects.PlayerGameObject;
import com.starpxy.util.Util;
import com.starpxy.util.Vector3f;

public class EnemyAgent implements Agent {

	private boolean isPlayerVisible = false;
	private int sightDistance = 300;
	private int shootRange = 200;
	private Vector3f toPlayer;
	private boolean isObstacleInFront;
	private boolean isObstacleBehind;
	private boolean isFalling;
	private boolean isJumping;
	private int direction;

	@Override
	public void sense(Object[] objects) {
		PlayerGameObject playerGameObject = (PlayerGameObject) objects[0];
		EnemyGameObject thisEnemy = (EnemyGameObject) objects[1];
		if (playerGameObject.getCentre().distanceOf(thisEnemy.getCentre()) <= sightDistance) {
			isPlayerVisible = true;
			toPlayer = thisEnemy.getCentre().MinusPoint(playerGameObject.getCentre());
		}
		if (thisEnemy.getRigidbody().isYFrozen()) {
			isObstacleInFront = true;
		}
		if (thisEnemy.getRigidbody().isXFrozen()) {
			isObstacleBehind = true;
		}
		if (!thisEnemy.isJumping() && thisEnemy.isGround() == false) {
			isFalling = true;
		} else {
			isFalling = false;
		}
		if (thisEnemy.isJumping()) {
			isJumping = true;
		} else {
			isJumping = false;
		}
		direction = thisEnemy.getHorizontalDirection();
	}

	@Override
	public int producePlan() {
		int plan = 0;
		// define the rules
		if (isPlayerVisible) {
			if (Math.abs(toPlayer.getX()) > shootRange) {
				if (toPlayer.getX() > 0) {
					plan += EnemyAI.SHOOT_LEFT;
				} else {
					plan += EnemyAI.SHOOT_RIGHT;
				}
			}
			if ((isObstacleBehind && direction == Util.BACK) || (isObstacleInFront && direction == Util.FORWARD)||(Math.abs(toPlayer.getX())==0&&toPlayer.getY()>10)) {
				plan += EnemyAI.JUMP;
			}
			if (toPlayer.getX() < 0) {
				plan += EnemyAI.MOVE_FORWARD;
			} else {
				plan += EnemyAI.MOVE_BACKWARD;
			}
		} else {
			// else walk randomly
			if (isFalling&&!isJumping) {
				if (direction == Util.FORWARD) {
					// turn around
					plan += EnemyAI.MOVE_BACKWARD;
				} else {
					// turn around
					plan += EnemyAI.MOVE_FORWARD;
				}
				plan += EnemyAI.JUMP;
			} else {
				if (direction == Util.FORWARD) {
					// keep going
					if (isObstacleInFront) {
						plan += EnemyAI.MOVE_BACKWARD;
					} else {
						plan += EnemyAI.MOVE_FORWARD;
					}
				} else {
					// keep going
					if (isObstacleBehind) {
						plan += EnemyAI.MOVE_FORWARD;
					} else {
						plan += EnemyAI.MOVE_BACKWARD;
					}
				}
			}
		}
		return plan;
	}

	@Override
	public void sendMessageToAll() {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendMessageTo(int agentID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getAgentID() {
		// TODO Auto-generated method stub
		return 0;
	}

}
