package com.starpxy.agents;

/**
 * An updated enemy agent
 * 
 * @author starp
 * created on 18/02/2021
 */

import com.starpxy.agents.base.Agent;

public class SimpleEnemyAgent implements Agent{

	@Override
	public void sense(Object[] objects) {
		
	}

	@Override
	public int producePlan() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void sendMessageToAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendMessageTo(int agentID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getAgentID() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
