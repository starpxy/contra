package com.starpxy.agents.base;

import java.util.HashMap;

public class Test {
	private int a;
	public static void main(String[] args) {
		HashMap<Integer, Test> map = new HashMap<Integer, Test>();
		Test test = new Test();
		test.a = 10;
		map.put(1, test);
		Test tempTest = map.get(1);
		tempTest.a = 20;
		System.out.println(test.a);
	}
}
