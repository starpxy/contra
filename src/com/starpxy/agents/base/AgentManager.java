package com.starpxy.agents.base;

import java.util.HashMap;

/**
 * A singleton manager class for manage the communication between agents.
 * 
 * @author starp
 *
 */
public class AgentManager {

	private HashMap<Integer, Agent> agents; // stores the references of agents
	private static AgentManager agentManager; // singleton manager
	private byte[] messageBuffer; // the messageBuffer stores all the message to be sent;

	public static AgentManager getInstance() {
		if (agentManager == null) {
			agentManager = new AgentManager();
		}
		return agentManager;
	}

	private AgentManager() {
		agents = new HashMap<Integer, Agent>();
	}

	/**
	 * register an agent to the map based on his ID
	 * @param agent
	 */
	public void registerAgent(Agent agent) {
		agents.put(agent.getAgentID(), agent);
	}
	
	/**
	 * destroy a agent according to its ID
	 * @param agentID
	 */
	public void destroyAgent(int agentID) {
		agents.remove(agentID);
	}

}
