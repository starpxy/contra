package com.starpxy.agents.base;

/**
 * The agent is actually a planner.
 * 
 * During each execution loop, the agent sense the environment then make a plan
 * according to the
 * 
 * @author starp
 *
 */
public interface Agent {

	/**
	 * Update the sense of the environment
	 * 
	 * @param objects
	 */
	public void sense(Object[] objects);

	/**
	 * Produce a plan according to the current condition
	 */
	public int producePlan();

	/**
	 * Send messages to all agents (Not applied in prototype.)
	 */
	public void sendMessageToAll();

	/**
	 * Send message to one agent
	 * 
	 * @param agentID
	 */
	public void sendMessageTo(int agentID);

	/**
	 * 
	 * @return the agent ID
	 */
	public int getAgentID();
	
	

}
