package com.starpxy.agents;

import com.starpxy.gameobjects.PlayerGameObject;
import com.starpxy.util.Point3f;

/**
 * This is a simple AI agent to kill the player/agent player
 * 
 * Goal: kill the player or AI player
 * 
 * Define "Kill": shoot a bullet hit the player or a directly knock onto the
 * player
 * 
 * Environment: player's position; obstacles;
 * 
 * Input to the AI: Environment
 * 
 * Output: the decision, including move forward/move backward/jump/shoot
 * 
 * @author xingyupan
 *
 */
public class EnemyAI {
	// the agent will see the player within the sight distance;
	// if the player is visible, the agent will run to the player;
	// other wise the agent will shoot user
	private static int sightDistance = 350;

	// These static final variables are the basic action that a AI can do and they
	// are used to help model detect which action is
	// triggered by the agent.
	public static final int MOVE_FORWARD = 10;
	public static final int MOVE_BACKWARD = 100;
	public static final int JUMP = 1000;
	public static final int SHOOT_LEFT = 10000;
	public static final int SHOOT_RIGHT = 100000;

	public static int produceSimplePlan(PlayerGameObject player, Point3f thisPos, boolean isXFrozen, boolean isYFrozen,
			boolean isJumping, boolean isShooting) {
		int plan = 0;
		if (player.getCentre().distanceOf(thisPos) <= sightDistance) {
			if (Math.abs(player.getCentre().getX() - thisPos.getX()) < 5
					&& thisPos.getY() > player.getCentre().getY()) {
				plan += JUMP;
			}
			// the agent can see the player
			else if (player.getCentre().getX() - thisPos.getX() > 10) {
				// player is on the right;
				if (isXFrozen) {
					// can not go left, then try to jump
					if (!isJumping) {
						plan += JUMP;
						plan += MOVE_FORWARD;
					} else {
						plan += MOVE_BACKWARD;
					}
				} else {
					plan += MOVE_FORWARD;
				}
			} else if (player.getCentre().getX() - thisPos.getX() < -10) {
				// player on the left;
				if (isYFrozen) {
					// cannot go right, then try to jump
					if (!isJumping) {
						plan += JUMP;
						plan += MOVE_BACKWARD;
					} else {
						plan += MOVE_FORWARD;
					}
				} else {
					plan += MOVE_BACKWARD;
				}
			} else {
				if (player.getCentre().getX() > thisPos.getX()) {
					plan += SHOOT_RIGHT;
				} else if (player.getCentre().getX() < thisPos.getX()) {
					plan += SHOOT_RIGHT;
				}
			}
		} else {
			// agent cannot see the users, shoot
			if (player.getCentre().getX() > thisPos.getX()) {
				// player on the right
				plan += SHOOT_RIGHT;
			} else {
				plan += SHOOT_LEFT;
			}
		}
		return plan;
	}
	
}
