package com.starpxy.util;

public class Util {

	public final static float g = 10f;
	public final static float G = 0.0000000000667f;
	public final static int FORWARD = 0;
	public final static int BACK = 1;
	public final static int UP = 2;
	public final static int DOWN = 3;
	public final static int DEFAULT = 7;
	public final static int RUN = 4;
	public final static int JUMP = 5;
	public final static int IDLE = 6;
	public final static int HURT = 8;
	public final static int DEFEATED = 9;
	public final static int DEAD = 10;
	public final static int SOUL = 11;
	public final static int PLAYING = 12;
	public final static int LOST = 13;
	public final static int WON = 14;
	public static enum GAME_MODE{
		SINGLE_PLAYER,
		DUO_PLAYERS
	};
	
	private static long synchronizedClock = 0;

	public static void updateClock() {
		synchronizedClock++;
	}

	public static long getCurrentTime() {
		return synchronizedClock;
	}
}
