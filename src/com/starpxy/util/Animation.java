package com.starpxy.util;

/**
 * 
 * @author xingyupan
 *
 */
public class Animation {
	private int numOfFrame;
	private Texture[] animationTextures;
	private long duration;
	private long animationStartTime;

	public Animation() {

	}

	public Animation(int numOfFrame) {
		super();
		this.numOfFrame = numOfFrame;
	}

	public Animation(int numOfFrame, Texture[] animationTextures, long duration) {
		super();
		this.numOfFrame = numOfFrame;
		this.animationTextures = animationTextures;
		this.duration = duration;
	}

	public int getNumOfFrame() {
		return numOfFrame;
	}

	public void setNumOfFrame(int numOfFrame) {
		this.numOfFrame = numOfFrame;
	}

	public Texture[] getAnimationTextures() {
		return animationTextures;
	}

	public void setAnimationTextures(Texture[] animationTextures) {
		this.animationTextures = animationTextures;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getAnimationStartTime() {
		return animationStartTime;
	}

	public void setAnimationStartTime(long animationStartTime) {
		this.animationStartTime = animationStartTime;
	}

}
