package com.starpxy.util;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * @author xingyupan
 *
 */
public class Texture {
	private String spritePath;
	private Image textureImage;
	private int x;
	private int y;
	private int width;
	private int height;

	public Texture(String spritePath, int x, int y, int width, int height) {
		super();
		File file = new File(spritePath);
		try {
			textureImage = ImageIO.read(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.spritePath = spritePath;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public Image getTextureImage() {
		return textureImage;
	}

	public String getSpritePath() {
		return spritePath;
	}

	public void setTextureImage(String spritePath) {
		this.spritePath = spritePath;
		File file = new File(spritePath);
		try {
			textureImage = ImageIO.read(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Animation loadAnimation(int frame) {
		Animation animation = new Animation(frame);
		Texture[] textures = new Texture[frame];
		for (int i = 0; i < frame; i++) {
			Texture tempTexture = new Texture(spritePath, x + i * width, y, width, height);
			textures[i] = tempTexture;
		}
		animation.setAnimationTextures(textures);
		return animation;
	}

}
