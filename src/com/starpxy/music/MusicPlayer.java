package com.starpxy.music;


/**
 * The player method is based on jLayer (from maven)
 * 
 * Repository URL: https://mvnrepository.com/artifact/javazoom/jlayer/1.0.1
 * 
 * The player will block the process so need use thread to play the music
 * 
 * Use Java thread pool to manage the players
 * 
 * @author xingyupan
 *
 */
public class MusicPlayer {
	private static MusicPlayer musicPlayer;
	private MusicPlayerThread gameBGMusicPlayerThread;
	private MusicPlayerThread startBGMusicPlayerThread;
	private MusicPlayerThread bossBGMusicPlayerThread;

	private MusicPlayer() {
		gameBGMusicPlayerThread = new MusicPlayerThread("res/music/gameBGM.mp3");
		startBGMusicPlayerThread = new MusicPlayerThread("res/music/startBGM.mp3");
		bossBGMusicPlayerThread = new MusicPlayerThread("res/music/bossBGM.mp3");
	}

	public static MusicPlayer getInstance() {
		if (musicPlayer == null) {
			musicPlayer = new MusicPlayer();
		}
		return musicPlayer;
	}

	public boolean isBossBGMEnd() {
		return bossBGMusicPlayerThread.isComplete();
	}

	public boolean isStartBGMEnd() {
		return startBGMusicPlayerThread.isComplete();
	}

	public void playGameBGM() {
		gameBGMusicPlayerThread = new MusicPlayerThread("res/music/gameBGM.mp3");
		gameBGMusicPlayerThread.start();
	}

	public void playStartBGM() {
		startBGMusicPlayerThread.start();
	}

	public void playBossBGM() {
//		bossBGMusicPlayerThread = new MusicPlayerThread("res/music/bossBGM.mp3");
		bossBGMusicPlayerThread.start();
	}

	public void stopBossBGM() {
		bossBGMusicPlayerThread.stopPlay();
	}

	public void playBossIntro() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/boss-intro.mp3");
		thread.start();
	}
	public void playWinSound() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/win-sound.mp3");
		thread.start();
	}

	public void playBossInjured() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/boss-injured.mp3");
		thread.start();
	}

	public void playBossOut() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/boss-out.mp3");
		thread.start();
	}

	public void playFlameGunEffect() {
		MusicPlayerThread shootPlayerThread = new MusicPlayerThread("res/music/flame-gun.mp3");
		shootPlayerThread.start();
	}

	public void playPauseSound() {
		MusicPlayerThread shootPlayerThread = new MusicPlayerThread("res/music/pause.mp3");
		shootPlayerThread.start();
	}
	public void playGenerateSound() {
		MusicPlayerThread shootPlayerThread = new MusicPlayerThread("res/music/generate.mp3");
		shootPlayerThread.start();
	}


	public void playPlayerDieSound() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/die-2.mp3");
		thread.start();
	}

	public void playEnemyDieSound() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/enemy_die_2.mp3");
		thread.start();
	}

	public void playLostSound() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/game-over.mp3");
		thread.start();
	}

	public void playHurtSound() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/hurt.mp3");
		thread.start();
	}

	public void playAmmoSound() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/power-up.mp3");
		thread.start();
	}
	
	public void playBossShootSound() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/boos-shoot.mp3");
		thread.start();
	}
	public void playBossDie() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/boss-defeat.mp3");
		thread.start();
	}
	

	public void playBoxExplosionSound() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/box-explosion.mp3");
		thread.start();
	}

	public void playJumpSound() {
		MusicPlayerThread thread = new MusicPlayerThread("res/music/jump.mp3");
		thread.start();
	}

	public void stopStartBGM() {
		startBGMusicPlayerThread.stopPlay();
	}

	public void stopGameBGM() {
		gameBGMusicPlayerThread.stopPlay();
	}
}
