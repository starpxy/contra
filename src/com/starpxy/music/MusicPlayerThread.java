package com.starpxy.music;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class MusicPlayerThread extends Thread {
	private Player player;

	public MusicPlayerThread(String path) {
		File file = new File(path);
		try {
			player = new Player(new FileInputStream(file));
		} catch (FileNotFoundException | JavaLayerException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		super.run();
		try {
			player.play();
		} catch (JavaLayerException e) {
			e.printStackTrace();
		}
		if (player.isComplete()) {
			player.close();
		}
	}

	public void stopPlay() {
		player.close();
	}

	public boolean isComplete() {
		return player.isComplete();
	}
}
