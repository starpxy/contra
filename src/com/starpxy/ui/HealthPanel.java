package com.starpxy.ui;

import java.awt.Graphics;

import com.starpxy.util.Texture;

public class HealthPanel {
	private Texture avatarTexture;
	private Texture healthTexture;
	private int margin = 30;
	private int padding = 20;

	public HealthPanel() {
		avatarTexture = new Texture("res/SpriteSheets/ui/player_icon_idle.png", 0, 0, 16, 13);
		healthTexture = new Texture("res/SpriteSheets/ui/ui_medal_life.png", 0, 0, 8, 13);
	}

	/**
	 * Draw the player avatar and 
	 * @param lifeNum
	 * @param g
	 */
	public void drawHealthPanel(int lifeNum, Graphics g) {
		g.drawImage(avatarTexture.getTextureImage(), margin, margin, margin + 64, margin + 52, avatarTexture.getX(),
				avatarTexture.getY(), avatarTexture.getX() + avatarTexture.getWidth(),
				avatarTexture.getY() + avatarTexture.getHeight(), null);
		for (int i = 0; i < lifeNum; i++) {
			g.drawImage(healthTexture.getTextureImage(), margin + 64 + padding * 2 + i * (32 + padding), margin,
					margin + 96 + padding * 2 + i * (32 + padding), margin + 52, healthTexture.getX(),
					healthTexture.getY(), healthTexture.getX() + healthTexture.getWidth(),
					healthTexture.getY() + healthTexture.getHeight(), null);
		}
	}

	public int getMargin() {
		return margin;
	}

	public int getPadding() {
		return padding;
	}
}
