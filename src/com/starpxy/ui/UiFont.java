package com.starpxy.ui;

public class UiFont {
	public static final int EXTRA_LARGE = 90;
	public static final int LARGE = 70;
	public static final int MEDIUM = 50;
	public static final int SMALL = 30;
	public static final int EXTRA_SMALL = 20;
	public static final int EXTRA_EXTRA_SMALL = 10;
}
