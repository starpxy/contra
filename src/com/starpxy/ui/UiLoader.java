package com.starpxy.ui;

import java.awt.Graphics;
import java.util.HashMap;

import com.starpxy.util.Texture;

/**
 * This class is to load the UI from characters or a string.
 * 
 * The method can draw different size of font:
 * 
 * ExtraLarge fount is 110 px;
 * 
 * Large font is 90 px;
 * 
 * Medium font is 70 px;
 * 
 * Small font is 40 px;
 * 
 * ExtraSmall font is 20 px;
 * 
 * @author xingyupan
 *
 */
public class UiLoader {
	private String spritePath = "res/SpriteSheets/ui/fnt_wonder32_custom.png";
	private HashMap<Character, Texture> textureMap = new HashMap<Character, Texture>();
	private static UiLoader uiLoader;

	private UiLoader() {
		char space = ' ';
		textureMap.put(space, new Texture(spritePath, 0, 0, 0, 0));
		for (int i = 0; i < 33; i++) {
			char temp = 0;
			if (i < 12) {
				temp = (char) (i + 97);
				if (i==9) {
					textureMap.put(temp, new Texture(spritePath, i * 32, 0, 18, 30));
				}
				else {
					textureMap.put(temp, new Texture(spritePath, i * 32, 0, 32, 30));
				}
			} else if (i < 21) {
				temp = (char) (i + 98);
				textureMap.put(temp, new Texture(spritePath, i * 32, 0, 32, 30));
			} else if (i < 22) {
				temp = (char) (i + 99);
				textureMap.put(temp, new Texture(spritePath, i * 32, 0, 32, 30));
			} else if (i < 23) {
				temp = (char) (i + 100);
				textureMap.put(temp, new Texture(spritePath, i * 32, 0, 32, 30));
			} else if (i < 33) {
				temp = (char) (i + 25);
				textureMap.put(temp, new Texture(spritePath, i * 32, 0, 32, 30));
			}
		}
		textureMap.put('m', new Texture(spritePath, 33 * 32, 0, 46, 30));
		textureMap.put('w', new Texture(spritePath, 33 * 32, 0, 46, 28));
	}

	public static UiLoader getInstance() {
		if (uiLoader == null) {
			uiLoader = new UiLoader();
		}
		return uiLoader;
	}

	public void drawFromString(String stringToWrite, Graphics g, int initX, int initY, int FontSize) {
		char[] charsToWrite = stringToWrite.toCharArray();
		for (int i = 0; i < charsToWrite.length; i++) {
			if (charsToWrite[i] == 'w') {
				g.drawImage(textureMap.get(charsToWrite[i]).getTextureImage(), initX + i * FontSize, initY+FontSize,
						initX + (i + 1) * FontSize, initY, textureMap.get(charsToWrite[i]).getX(),
						textureMap.get(charsToWrite[i]).getY(),
						textureMap.get(charsToWrite[i]).getX() + textureMap.get(charsToWrite[i]).getWidth(),
						textureMap.get(charsToWrite[i]).getY() + textureMap.get(charsToWrite[i]).getHeight(), null);
			}
			else {
				g.drawImage(textureMap.get(charsToWrite[i]).getTextureImage(), initX + i * FontSize, initY,
						initX + (i + 1) * FontSize, initY + FontSize, textureMap.get(charsToWrite[i]).getX(),
						textureMap.get(charsToWrite[i]).getY(),
						textureMap.get(charsToWrite[i]).getX() + textureMap.get(charsToWrite[i]).getWidth(),
						textureMap.get(charsToWrite[i]).getY() + textureMap.get(charsToWrite[i]).getHeight(), null);
			}
		}
	}

}
