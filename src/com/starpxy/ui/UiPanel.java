package com.starpxy.ui;

import java.awt.Graphics;

import com.starpxy.util.Util;

public class UiPanel {
	private long startTime;
	private long lastTime;
	private String stringToWrite;
	private int x;
	private int y;
	private int fontSize;
	private boolean isAlwaysOn = false;

	public UiPanel(int x, int y, int fontSize, long lastTime, String stringToWrite, boolean isAlawaysOn) {
		startTime = Util.getCurrentTime();
		this.x = x;
		this.y = y;
		this.fontSize = fontSize;
		this.lastTime = lastTime;
		this.isAlwaysOn = isAlawaysOn;
		this.stringToWrite = stringToWrite;
	}

	public void renderUI(Graphics g) {
		UiLoader.getInstance().drawFromString(stringToWrite, g, x, y, fontSize);
	}

	public boolean isAlwaysOn() {
		return isAlwaysOn;
	}

	public long getLastTime() {
		return lastTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}
}
