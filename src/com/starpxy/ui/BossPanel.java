package com.starpxy.ui;

import java.awt.Graphics;

import com.starpxy.util.Texture;

public class BossPanel {
	private Texture avatarTexture;
	private Texture healthTexture;
	private Texture emptyHealthTexture;
	private int margin = 30;
	private int padding = 10;

	public BossPanel() {
		avatarTexture = new Texture("res/SpriteSheets/boss/BossHead-static.png", 0, 0, 47, 67);
		healthTexture = new Texture("res/SpriteSheets/ui/life_unit.png", 0, 0, 3, 5);
		emptyHealthTexture = new Texture("res/SpriteSheets/ui/life_empty.png", 0, 0, 3, 5);
	}

	public void drawBossPanel(int totalHealth, int currentHealth, Graphics g) {
		g.drawImage(avatarTexture.getTextureImage(), 1280 - margin - 50, margin, 1280 - margin, margin + 70,
				avatarTexture.getX(), avatarTexture.getY(), avatarTexture.getX() + avatarTexture.getWidth(),
				avatarTexture.getY() + avatarTexture.getHeight(), null);
		int num = currentHealth * 10 / totalHealth;
		for (int i = 0; i < 10; i++) {
			if (i < num) {
				g.drawImage(healthTexture.getTextureImage(), 1280 - margin - 50 - padding * 2 - i * (30 + padding),
						margin, 1280 - margin - 50 - 30 - padding * 2 - i * (30 + padding), margin + 70,
						healthTexture.getX(), healthTexture.getY(), healthTexture.getX() + healthTexture.getWidth(),
						healthTexture.getY() + healthTexture.getHeight(), null);
			} else {
				g.drawImage(emptyHealthTexture.getTextureImage(), 1280 - margin - 50 - padding * 2 - i * (30 + padding),
						margin, 1280 - margin - 50 - 30 - padding * 2 - i * (30 + padding), margin + 70,
						emptyHealthTexture.getX(), emptyHealthTexture.getY(),
						emptyHealthTexture.getX() + emptyHealthTexture.getWidth(),
						emptyHealthTexture.getY() + emptyHealthTexture.getHeight(), null);
			}

		}
	}

	public int getMargin() {
		return margin;
	}

	public int getPadding() {
		return padding;
	}
}
