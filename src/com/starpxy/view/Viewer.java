package com.starpxy.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.starpxy.gameobjects.AmmoGameObject;
import com.starpxy.gameobjects.BossGameObject;
import com.starpxy.gameobjects.BulletGameObject;
import com.starpxy.gameobjects.EffectiveGameObject;
import com.starpxy.gameobjects.EnemyBulletGameObject;
import com.starpxy.gameobjects.EnemyGameObject;
import com.starpxy.gameobjects.GameObject;
import com.starpxy.gameobjects.PlayerGameObject;
import com.starpxy.model.Model;
import com.starpxy.ui.BossPanel;
import com.starpxy.ui.HealthPanel;
import com.starpxy.ui.UiPanel;
import com.starpxy.util.Animation;
import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Util;

/*
 * Created by Abraham Campbell on 15/01/2020.
 *   Copyright (c) 2020  Abraham Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
   
   (MIT LICENSE ) e.g do what you want with this :-) 
 */
public class Viewer extends JPanel {

	private static final long serialVersionUID = 2646628037451993866L;
	Model gameworld = new Model();
	private HealthPanel healthPanel;
	private HealthPanel extraHealthPanel;
	private BossPanel bossPanel;
	private SettingPanel settingPanel;

	public Viewer(Model World) {
		this.gameworld = World;
		healthPanel = new HealthPanel();
		extraHealthPanel = new HealthPanel();
		bossPanel = new BossPanel();
		settingPanel = new SettingPanel(gameworld);
		settingPanel.setBounds(0, 0, 1280, 720);
		settingPanel.setVisible(false);
		this.add(settingPanel);
	}

	public Viewer(LayoutManager layout) {
		super(layout);
	}

	public Viewer(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
	}

	public Viewer(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
	}

	public void updateview() {
		this.repaint();
	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		if (gameworld.isEnd()) {
			if (gameworld.getGameStatus() == Util.WON) {
				drawWonPage(g);
			} else if (gameworld.getGameStatus() == Util.LOST) {
				drawLostPage(g);
			}
			return;
		}

		// Draw background
		drawBackground(g);
		drawNewBG(g);
		if (gameworld.isBoss()) {
			drawBoss(g);
		}
		drawEnemies(g);

		// Draw map
		drawMap(g);

		// Draw player
		drawPlayer(gameworld.getPlayerGameObject(), g);

		// Draw player bullets
		drawPlayerBullets(g);

		drawEnemyBullets(g);
		drawEffect(g);

		// Draw health panel
		drawHealthPanel(g);
		drawAmmo(g);
		// Draw UI
		drawUI(g);
//		drawSettingPanel();
		if (gameworld.isBoss()) {
			bossPanel.drawBossPanel(gameworld.getMap().getBossHealth(),gameworld.getBossGameObject().getHealth(), g);
		}
		if (gameworld.isBlack()) {
			g.setColor(Color.black);
			g.fillRect(0, 0, 1280, 720);
		}
	}
	
	private void drawSettingPanel() {
		if (gameworld.isSettingToggled()) {
			settingPanel.setVisible(false);
		}
		else {
			settingPanel.setVisible(true);
		}
	}

	private void drawWonPage(Graphics g) {
		File bgFile = new File("res/SpriteSheets/titlescreen/CreditsScreen.png");
		try {
			Image myImage = ImageIO.read(bgFile);
			g.drawImage(myImage, 0, 0, 1280, 720, 0, 0, 320, 180, null);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void drawLostPage(Graphics g) {
		File bgFile = new File("res/SpriteSheets/titlescreen/GameOverScreen-still.png");
		try {
			Image myImage = ImageIO.read(bgFile);
			g.drawImage(myImage, 0, 0, 1280, 720, 0, 0, 320, 180, null);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void drawNewBG(Graphics g) {
		File bgFile = new File("res/SpriteSheets/background/background_cave.png");
		try {
			Image myImage = ImageIO.read(bgFile);
			g.drawImage(myImage, gameworld.getMap().getNewBackgroundX() - gameworld.getWindowPos(), 0,
					1300 + gameworld.getMap().getNewBackgroundX() - gameworld.getWindowPos(), 900, 0, 0, 400, 300,
					null);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void drawAmmo(Graphics g) {
		for (AmmoGameObject ammoGameObject : gameworld.getAmmoList()) {
			Texture backTexture = ammoGameObject.getTexture();
			Texture frontTexture = ammoGameObject.getTypeTexture();
			int x = (int) (ammoGameObject.getCentre().getX() - ammoGameObject.getWidth() / 2);
			int y = (int) (ammoGameObject.getCentre().getY() - ammoGameObject.getHeight() / 2);
			g.drawImage(backTexture.getTextureImage(), (int) (x - gameworld.getWindowPos()), (int) (y),
					(int) (x + ammoGameObject.getWidth() - gameworld.getWindowPos()),
					(int) (y + ammoGameObject.getHeight()), backTexture.getX(), backTexture.getY(),
					backTexture.getX() + backTexture.getWidth(), backTexture.getY() + backTexture.getHeight(), null);
			g.drawImage(frontTexture.getTextureImage(),
					(int) (x + ammoGameObject.getTypeStartPos().getX() - gameworld.getWindowPos()),
					(int) (y + ammoGameObject.getTypeStartPos().getY()),
					(int) (x + ammoGameObject.getTypeEndPos().getX() - gameworld.getWindowPos()),
					(int) (y + ammoGameObject.getTypeEndPos().getY()), frontTexture.getX(), frontTexture.getY(),
					frontTexture.getX() + frontTexture.getWidth(), frontTexture.getY() + frontTexture.getHeight(),
					null);
		}
	}

	private void drawBoss(Graphics g) {
		BossGameObject boss = gameworld.getBossGameObject();
		if (boss.getStatus() == 0) {
			Animation animation = boss.getBossIntroBGAnimation();
			int currentFrame = (int) (((Util.getCurrentTime() - boss.getStartTimestamp()) % animation.getDuration())
					* animation.getNumOfFrame() / animation.getDuration());
			Texture texture = animation.getAnimationTextures()[currentFrame];
			g.drawImage(texture.getTextureImage(), 0, 0, 1280, 720, texture.getX(), texture.getY(),
					texture.getX() + texture.getWidth(), texture.getY() + texture.getHeight(), null);
		}
		boss.renderBossIdle(g);
		boss.renderCache(g);
	}

	private void drawUI(Graphics g) {
		for (UiPanel uiPanel : gameworld.getUiPanels()) {
			if (uiPanel.isAlwaysOn() || uiPanel.getStartTime() + uiPanel.getLastTime() > Util.getCurrentTime()) {
				uiPanel.renderUI(g);
			} else {
				gameworld.removePanel(uiPanel);
			}
		}
	}

	/**
	 * Render the background color, background jungle and clouds
	 * 
	 * @param g is the graphics instance
	 */
	private void drawBackground(Graphics g) {
		if (gameworld.isBoss()) {
			File bgFile = new File("res/SpriteSheets/boss/BossScreen_rockBG.png");
			try {
				Image myImage = ImageIO.read(bgFile);
				g.drawImage(myImage, 0, 0, 1280, 720, 0, 0, 320, 180, null);

			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			File bgColor = new File("res/SpriteSheets/background/background_color.png"); // should work okay on OSX and
			// Linux but check if you
			// your running this without an IDE
			File bgTrees = new File("res/SpriteSheets/background/jungle_paralax_bg1.png");
			File bgTrees2 = new File("res/SpriteSheets/background/jungle_paralax_bg2.png");
			File cloud = new File("res/SpriteSheets/background/clouds1.png");
			File fgTree = new File("res/SpriteSheets/background/jungle_bg_trees.png");
			try {
				Image myImage = ImageIO.read(bgColor);
				// draw bg color
				g.drawImage(myImage, 0, 0, 1280, 720, 0, 0, 384, 216, null);
				// draw floating cloud
				myImage = ImageIO.read(cloud);
				if (gameworld.getMap().getNewBackgroundX() - gameworld.getWindowPos() < 1280) {
					g.drawImage(myImage,
							(int) (-1280 + (Util.getCurrentTime() / 2 % 1280)) + gameworld.getMap().getNewBackgroundX()
									- 1280 - gameworld.getWindowPos(),
							0, (int) ((Util.getCurrentTime() / 2 % 1280)) + gameworld.getMap().getNewBackgroundX()
									- 1280 - gameworld.getWindowPos(),
							300, 0, 0, 173, 38, null);
					g.drawImage(myImage,
							(int) ((Util.getCurrentTime() / 2 % 1280)) + gameworld.getMap().getNewBackgroundX() - 1280
									- gameworld.getWindowPos(),
							0, (int) (1280 + (Util.getCurrentTime() / 2 % 1280))
									+ gameworld.getMap().getNewBackgroundX() - 1280 - gameworld.getWindowPos(),
							300, 0, 0, 173, 38, null);
					myImage = ImageIO.read(bgTrees);
					g.drawImage(myImage, 0 + gameworld.getMap().getNewBackgroundX() - 1280 - gameworld.getWindowPos(),
							0, 1280 + gameworld.getMap().getNewBackgroundX() - 1280 - gameworld.getWindowPos(), 720, 0,
							0, 384, 216, null);
					myImage = ImageIO.read(bgTrees2);
					g.drawImage(myImage, 0 + gameworld.getMap().getNewBackgroundX() - 1280 - gameworld.getWindowPos(),
							0, 1280 + gameworld.getMap().getNewBackgroundX() - 1280 - gameworld.getWindowPos(), 720, 0,
							0, 384, 216, null);
					myImage = ImageIO.read(fgTree);
					g.drawImage(myImage, 0 + gameworld.getMap().getNewBackgroundX() - 1280 - gameworld.getWindowPos(),
							200, 600 + gameworld.getMap().getNewBackgroundX() - 1280 - gameworld.getWindowPos(), 656, 0,
							0, 144, 112, null);
					g.drawImage(myImage, 850 + gameworld.getMap().getNewBackgroundX() - 1280 - gameworld.getWindowPos(),
							300, 1400 + gameworld.getMap().getNewBackgroundX() - 1280 - gameworld.getWindowPos(), 756,
							0, 0, 144, 112, null);
				} else {
					g.drawImage(myImage, (int) (-1280 + (Util.getCurrentTime() / 2 % 1280)), 0,
							(int) ((Util.getCurrentTime() / 2 % 1280)), 300, 0, 0, 173, 38, null);
					g.drawImage(myImage, (int) ((Util.getCurrentTime() / 2 % 1280)), 0,
							(int) (1280 + (Util.getCurrentTime() / 2 % 1280)), 300, 0, 0, 173, 38, null);
					myImage = ImageIO.read(bgTrees);
					g.drawImage(myImage, 0, 0, 1280, 720, 0, 0, 384, 216, null);
					myImage = ImageIO.read(bgTrees2);
					g.drawImage(myImage, 0, 0, 1280, 720, 0, 0, 384, 216, null);
					myImage = ImageIO.read(fgTree);
					g.drawImage(myImage, 0, 200, 600, 656, 0, 0, 144, 112, null);
					g.drawImage(myImage, 850, 300, 1400, 756, 0, 0, 144, 112, null);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void drawHealthPanel(Graphics g) {
		healthPanel.drawHealthPanel(gameworld.getPlayerGameObject().getLifeNum(), g);
	}

	/**
	 * Draw enemies
	 * 
	 * @param g
	 */
	private void drawEnemies(Graphics g) {
		for (EnemyGameObject enemy : gameworld.getEnemies()) {
			Point3f pos = enemy.getCentre();
			int x = (int) pos.getX() - enemy.getWidth() / 2;
			int y = (int) pos.getY() - enemy.getHeight() / 2;
			Animation animation = enemy.getAnimation();
			int currentFrame = (int) ((Util.getCurrentTime() % animation.getDuration()) * animation.getNumOfFrame()
					/ animation.getDuration());
			if (enemy.isDead()) {
				currentFrame = (int) (((Util.getCurrentTime() - enemy.getDieTime()) % animation.getDuration())
						* animation.getNumOfFrame() / animation.getDuration());
			}
			Texture currentTexture = animation.getAnimationTextures()[currentFrame];

			if (enemy.getHorizontalDirection() == Util.FORWARD) {
				g.drawImage(currentTexture.getTextureImage(), x - gameworld.getWindowPos(), y,
						x + enemy.getWidth() - gameworld.getWindowPos(), y + enemy.getHeight(), currentTexture.getX(),
						currentTexture.getY(), currentTexture.getX() + currentTexture.getWidth(),
						currentTexture.getY() + currentTexture.getHeight(), null);
			} else {

				g.drawImage(currentTexture.getTextureImage(), x - gameworld.getWindowPos() + enemy.getWidth(), y,
						x - gameworld.getWindowPos(), y + enemy.getHeight(), currentTexture.getX(),
						currentTexture.getY(), currentTexture.getX() + currentTexture.getWidth(),
						currentTexture.getY() + currentTexture.getHeight(), null);
			}
		}
	}

	/**
	 * Render the map from model
	 * 
	 * @param g is the graphics instance
	 */
	private void drawMap(Graphics g) {
		CopyOnWriteArrayList<GameObject> mapList = gameworld.getMapList();
		for (GameObject temp : mapList) {
			Texture texture = temp.getTexture();
			Point3f center = temp.getCentre();
			int x = (int) (center.getX() - temp.getWidth() / 2);
			int y = (int) (center.getY() - temp.getHeight() / 2);
			g.drawImage(texture.getTextureImage(), x - gameworld.getWindowPos(), y,
					x + temp.getWidth() - gameworld.getWindowPos(), y + temp.getHeight(), texture.getX(),
					texture.getY(), texture.getX() + texture.getWidth(), texture.getY() + texture.getHeight(), null);
		}
	}

	/**
	 * Render the bullet by the users one by one
	 * 
	 * @param bulletGameObject is the bullet gameobject
	 * @param g                is the Graphics instance
	 */
	private void drawPlayerBullets(Graphics g) {
		for (BulletGameObject bulletGameObject : gameworld.getPlayerBullets()) {
			Point3f pos = bulletGameObject.getCentre();
			int width = bulletGameObject.getWidth();
			int height = bulletGameObject.getHeight();
			int x = (int) pos.getX() - width / 2;
			int y = (int) pos.getY() - height / 2;
			Animation animation = bulletGameObject.getAnimation();
			int currentFrame = (int) (((Util.getCurrentTime() - animation.getAnimationStartTime())
					% animation.getDuration()) * animation.getNumOfFrame() / animation.getDuration());
			Texture currentTexture = animation.getAnimationTextures()[currentFrame];
			g.drawImage(currentTexture.getTextureImage(), x - gameworld.getWindowPos(), y,
					x + width - gameworld.getWindowPos(), y + height, currentTexture.getX(), currentTexture.getY(),
					currentTexture.getX() + currentTexture.getWidth(),
					currentTexture.getY() + currentTexture.getHeight(), null);
		}
	}

	private void drawEnemyBullets(Graphics g) {
		for (EnemyBulletGameObject bulletGameObject : gameworld.getEnemyBullets()) {
			Point3f pos = bulletGameObject.getCentre();
			int width = bulletGameObject.getWidth();
			int height = bulletGameObject.getHeight();
			int x = (int) pos.getX() - width / 2;
			int y = (int) pos.getY() - height / 2;
			Animation animation = bulletGameObject.getAnimation();
			int currentFrame = (int) (((Util.getCurrentTime() - animation.getAnimationStartTime())
					% animation.getDuration()) * animation.getNumOfFrame() / animation.getDuration());
			Texture currentTexture = animation.getAnimationTextures()[currentFrame];
			g.drawImage(currentTexture.getTextureImage(), x - gameworld.getWindowPos(), y,
					x + width - gameworld.getWindowPos(), y + height, currentTexture.getX(), currentTexture.getY(),
					currentTexture.getX() + currentTexture.getWidth(),
					currentTexture.getY() + currentTexture.getHeight(), null);
		}
	}

	private void drawEffect(Graphics g) {
		for (EffectiveGameObject gameObject : gameworld.getEffectList()) {
			Point3f pos = gameObject.getCentre();
			int x = (int) pos.getX() - gameObject.getWidth() / 2;
			int y = (int) pos.getY() - gameObject.getHeight() / 2;
			Animation animation = gameObject.getAnimation();
			int currentFrame = (int) (((Util.getCurrentTime() - gameObject.getStartTime()) % animation.getDuration())
					* animation.getNumOfFrame() / animation.getDuration());
			Texture currentTexture = animation.getAnimationTextures()[currentFrame];
			if (gameObject.getDirection() == Util.FORWARD) {
				g.drawImage(currentTexture.getTextureImage(), x - gameworld.getWindowPos(), y,
						x + gameObject.getWidth() - gameworld.getWindowPos(), y + gameObject.getHeight(),
						currentTexture.getX(), currentTexture.getY(), currentTexture.getX() + currentTexture.getWidth(),
						currentTexture.getY() + currentTexture.getHeight(), null);
			} else {
				g.drawImage(currentTexture.getTextureImage(), x + gameObject.getWidth() - gameworld.getWindowPos(), y,
						x - gameworld.getWindowPos(), y + gameObject.getHeight(), currentTexture.getX(),
						currentTexture.getY(), currentTexture.getX() + currentTexture.getWidth(),
						currentTexture.getY() + currentTexture.getHeight(), null);
			}

		}
	}

	/**
	 * Render the player game object and corresponding animation
	 * 
	 * @param player is the player gameobject
	 * @param g      is the graphics instance
	 */
	private void drawPlayer(PlayerGameObject player, Graphics g) {
		// render half frames when unbreakable
		if (player.isUnbreakable()) {
			if (Util.getCurrentTime() % 10 < 5) {
				return;
			}
		}
		Point3f pos = player.getCentre();
		int x = (int) pos.getX() - player.getWidth() / 2;
		int y = (int) pos.getY() - player.getHeight() / 2;
		Animation animation = player.getAnimation();
		long initTime = 0;
		if (player.getStatus() == Util.DEFEATED) {
			initTime = player.getDieTimestamp();
		} else if (player.getStatus() == Util.HURT) {
			initTime = player.getLastHurtTimestamp();
		}
		int currentFrame = (int) (((Util.getCurrentTime() - initTime) % animation.getDuration())
				* animation.getNumOfFrame() / animation.getDuration());
		Texture currentTexture = animation.getAnimationTextures()[currentFrame];

		if (player.getHorizontalDirection() == Util.FORWARD) {
			g.drawImage(currentTexture.getTextureImage(), x - gameworld.getWindowPos(), y,
					x + player.getWidth() - gameworld.getWindowPos(), y + player.getHeight(), currentTexture.getX(),
					currentTexture.getY(), currentTexture.getX() + currentTexture.getWidth(),
					currentTexture.getY() + currentTexture.getHeight(), null);
		} else {
			g.drawImage(currentTexture.getTextureImage(), x + player.getWidth() - gameworld.getWindowPos(), y,
					x - gameworld.getWindowPos(), y + player.getHeight(), currentTexture.getX(), currentTexture.getY(),
					currentTexture.getX() + currentTexture.getWidth(),
					currentTexture.getY() + currentTexture.getHeight(), null);
		}
	}

}

/*
 * 
 * 
 * VIEWER HMD into the world
 * 
 * . . . .. .........~++++.. . . . . ....,++??+++?+??+++?++?7ZZ7.. . . . .
 * .+?+???++++???D7I????Z8Z8N8MD7I?=+O$.. ..
 * ........ZOZZ$7ZZNZZDNODDOMMMMND8$$77I??I?+?+=O . . ..
 * ...7$OZZ?788DDNDDDDD8ZZ7$$$7I7III7??I?????+++=+~.
 * ...8OZII?III7II77777I$I7II???7I??+?I?I?+?+IDNN8??++=...
 * ....OOIIIII????II?I??II?I????I?????=?+Z88O77ZZO8888OO?++,......
 * ..OZI7III??II??I??I?7ODM8NN8O8OZO8DDDDDDDDD8DDDDDDDDNNNOZ= ...... ..
 * ..OZI?II7I?????+????+IIO8O8DDDDD8DNMMNNNNNDDNNDDDNDDNNNNNNDD$,.........
 * ,ZII77II?III??????DO8DDD8DNNNNNDDMDDDDDNNDDDNNNDNNNNDNNNNDDNDD+....... .
 * 7Z??II7??II??I??IOMDDNMNNNNNDDDDDMDDDDNDDNNNNNDNNNNDNNDMNNNNNDDD,...... .
 * ..IZ??IIIII777?I?8NNNNNNNNNDDDDDDDDNDDDDDNNMMMDNDMMNNDNNDMNNNNNNDDDD.....
 * .$???I7IIIIIIINNNNNNNNNNNDDNDDDDDD8DDDDNM888888888DNNNNNNDNNNNNNDDO.....
 * $+??IIII?II?NNNNNMMMMMDN8DNNNDDDDZDDNN?D88I==INNDDDNNDNMNNMNNNNND8:.....
 * ....$+??III??I+NNNNNMMM88D88D88888DDDZDDMND88==+=NNNNMDDNNNNNNMMNNNNND8......
 * .......8=+????III8NNNNMMMDD8I=~+ONN8D8NDODNMN8DNDNNNNNNNM8DNNNNNNMNNNNDDD8...
 * .. .
 * ......O=??IIIIIMNNNMMMDDD?+=?ONNNN888NMDDM88MNNNNNNNNNMDDNNNMNNNMMNDNND8.....
 * .
 * ........,+++???IINNNNNMMDDMDNMNDNMNNM8ONMDDM88NNNNNN+==ND8NNNDMNMNNNNNDDD8...
 * ...
 * ......,,,:++??I?ONNNNNMDDDMNNNNNNNNMM88NMDDNN88MNDN==~MD8DNNNNNMNMNNNDND8O...
 * ...
 * ....,,,,:::+??IIONNNNNNNDDMNNNNNO+?MN88DN8DDD888DNMMM888DNDNNNNMMMNNDDDD8,...
 * . .
 * ...,,,,::::~+?+?NNNNNNNMD8DNNN++++MNO8D88NNMODD8O88888DDDDDDNNMMMNNNDDD8.....
 * ...
 * ..,,,,:::~~~=+??MNNNNNNNND88MNMMMD888NNNNNNNMODDDDDDDDND8DDDNNNNNNDDD8,......
 * ...
 * ..,,,,:::~~~=++?NMNNNNNNND8888888O8DNNNNNNMMMNDDDDDDNMMNDDDOO+~~::,,,........
 * ..
 * ..,,,:::~~~~==+?NNNDDNDNDDNDDDDDDDDNNND88OOZZ$8DDMNDZNZDZ7I?++~::,,,.........
 * ...
 * ..,,,::::~~~~==7DDNNDDD8DDDDDDDD8DD888OOOZZ$$$7777OOZZZ$7I?++=~~:,,,.........
 * ..,,,,::::~~~~=+8NNNNNDDDMMMNNNNNDOOOOZZZ$$$77777777777II?++==~::,,,...... .
 * ..
 * ...,,,,::::~~~~=I8DNNN8DDNZOM$ZDOOZZZZ$$$7777IIIIIIIII???++==~~::,,........ .
 * ....,,,,:::::~~~~+=++?I$$ZZOZZZZZ$$$$$777IIII?????????+++==~~:::,,,...... ..
 * .....,,,,:::::~~~~~==+?II777$$$$77777IIII????+++++++=====~~~:::,,,........
 * ......,,,,,:::::~~~~==++??IIIIIIIII?????++++=======~~~~~~:::,,,,,,.......
 * .......,,,,,,,::::~~~~==+++???????+++++=====~~~~~~::::::::,,,,,..........
 * .........,,,,,,,,::::~~~======+======~~~~~~:::::::::,,,,,,,,............
 * .........,.,,,,,,,,::::~~~~~~~~~~:::::::::,,,,,,,,,,,...............
 * ..........,..,,,,,,,,,,::::::::::,,,,,,,,,.,....................
 * .................,,,,,,,,,,,,,,,,.......................
 * .................................................
 * .................................... .................... .
 * 
 * 
 * GlassGiant.com
 */
