package com.starpxy.view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class DrawPanel extends JPanel {

	/**
	 * 
	 */
	private String path = null;
	private int[] imgBorder = null;
	private int[] panelBorder = null;

	/**
	 * set x1,y1,x2,y2
	 * 
	 * @param panelBorder: x1,y1,x2,y2
	 */
	public void setPanelBorder(int[] panelBorder) {
		this.panelBorder = panelBorder;
	}

	public void setImgBorder(int[] imgBorder) {
		this.imgBorder = imgBorder;
	}

	public void setPath(String path) {
		this.path = path;
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		File texture = new File(path);
		try {
			Image image = ImageIO.read(texture);
			g.drawImage(image, panelBorder[0], panelBorder[1], panelBorder[2], panelBorder[3], imgBorder[0],
					imgBorder[1], imgBorder[2], imgBorder[3], null);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
