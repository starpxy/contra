package com.starpxy.view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import com.starpxy.model.Model;

/**
 * Setting Panel
 * 
 * @author starp
 *
 */
public class SettingPanel extends JPanel {

	private File bgTexture;
	private File introTexture;
	private File bgTrees;
	private File bgTrees2;
	private File onFile;
	private File offFile;
	private Model gameworld;

	private static final long serialVersionUID = 2552803929953345692L;

	public SettingPanel(Model gameworld){
		bgTexture = new File("res/SpriteSheets/background/background_color.png");
		introTexture = new File("res/star_sprites/intro.png");
		bgTrees = new File("res/SpriteSheets/background/jungle_paralax_bg1.png");
		bgTrees2  = new File("res/SpriteSheets/background/jungle_paralax_bg2.png");
		onFile = new File("res/star_sprites/on.png");
		offFile = new File("res/star_sprites/off.png");
		this.gameworld = gameworld;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		drawBackground(g);
		drawMusicButton(g);
	}

	private void drawMusicButton(Graphics g) {
		boolean isMute = false;
		if (gameworld!=null) {
			isMute = gameworld.isMute();
		}
		try {
			Image image;
			if (!isMute) {
				image = ImageIO.read(onFile);
			}
			else {
				image = ImageIO.read(offFile);
			}
			g.drawImage(image, 850, 435, 1000, 485, 0, 0, 300, 100, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void drawBackground(Graphics g) {
		try {
			Image image = ImageIO.read(bgTexture);
			g.drawImage(image, 0, 0, 1280, 720, 0, 0, 320, 180, null);
			image = ImageIO.read(bgTrees);
			g.drawImage(image, 0, 0, 1280, 720, 0, 0, 384, 216, null);
			image = ImageIO.read(bgTrees2);
			g.drawImage(image, 0, 0, 1280, 720, 0, 0, 384, 216, null);
			image = ImageIO.read(introTexture);
			g.drawImage(image, 0, 0, 1280, 720, 0, 0, 1280, 720, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
