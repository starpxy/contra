package com.starpxy.view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * A UI panel to show the start views
 * @author starp
 *
 */
public class StartViewer extends JPanel {

	private static final long serialVersionUID = 1L;
	private String bgPath = "res/SpriteSheets/titlescreen/main_title_bg.png";
	private String titlePath = "res/SpriteSheets/titlescreen/main_title_title.png";
	private String opt1Path = "res/star_sprites/opt1.png";
	private String opt2Path = "res/star_sprites/opt2.png";
	private String opt3Path = "res/star_sprites/opt3.png";
	private String pointerPath = "res/star_sprites/pointer.png";
	private int currentOption = 0;

	public void setCurrentOption(int currentOption) {
		this.currentOption = currentOption;
	}

	public int getCurrentOption() {
		return currentOption;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawBackground(g);
		drawPointer(g);
	}

	private void drawPointer(Graphics g) {
		File textureFile = new File(pointerPath);
		try {
			Image image = ImageIO.read(textureFile);
			int y = 0;
			switch (currentOption) {
			case 0:
				y = 300;
				break;
			case 1:
				y = 350;
				break;
//			case 2:
//				y = 400;
//				break;
			default:
				y = 300;
				break;
			}
			g.drawImage(image, 500, y, 550, y + 50, 0, 0, 50, 50, null);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void drawBackground(Graphics g) {
		File bgTexture = new File(bgPath);
		File titleTexture = new File(titlePath);
		File opt1Texture = new File(opt1Path);
		File opt2Texture = new File(opt2Path);
		File opt3Texture = new File(opt3Path);
		try {
			Image image = ImageIO.read(bgTexture);
			g.drawImage(image, 0, 0, 1280, 720, 0, 0, 320, 180, null);
			image = ImageIO.read(titleTexture);
			g.drawImage(image, 350, 20, 950, 300, 0, 0, 215, 113, null);
			image = ImageIO.read(opt1Texture);
			g.drawImage(image, 550, 300, 700, 350, 0, 0, 300, 100, null);
			image = ImageIO.read(opt2Texture);
			g.drawImage(image, 550, 350, 700, 400, 0, 0, 300, 100, null);
//			image = ImageIO.read(opt3Texture);
//			g.drawImage(image, 550, 400, 850, 450, 0, 0, 300, 50, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
