package com.starpxy.model;

import java.util.concurrent.CopyOnWriteArrayList;

import javax.security.auth.x500.X500Principal;

import com.starpxy.agents.EnemyAI;
import com.starpxy.controller.Controller;
import com.starpxy.gameobjects.AmmoGameObject;
import com.starpxy.gameobjects.BasicGroundUnitGameObject;
import com.starpxy.gameobjects.BossGameObject;
import com.starpxy.gameobjects.BoxGameObject;
import com.starpxy.gameobjects.BulletGameObject;
import com.starpxy.gameobjects.CaveGameObject;
import com.starpxy.gameobjects.EffectiveGameObject;
import com.starpxy.gameobjects.EnemyBulletGameObject;
import com.starpxy.gameobjects.EnemyGameObject;
import com.starpxy.gameobjects.GameObject;
import com.starpxy.gameobjects.PlantGameObject;
import com.starpxy.gameobjects.PlayerGameObject;
import com.starpxy.gameobjects.ShelterGameObject;
import com.starpxy.map.Map;
import com.starpxy.map.MapLoader;
import com.starpxy.music.MusicPlayer;
import com.starpxy.physics2D.SimpleRectRigidbody;
import com.starpxy.ui.UiFont;
import com.starpxy.ui.UiPanel;
import com.starpxy.util.Animation;
import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Util;
import com.starpxy.util.Vector3f;
import com.starpxy.view.SettingPanel;
import com.studiohartman.jamepad.ControllerAxis;
import com.studiohartman.jamepad.ControllerButton;
import com.studiohartman.jamepad.ControllerIndex;
import com.studiohartman.jamepad.ControllerState;
import com.studiohartman.jamepad.ControllerUnpluggedException;

/*
 * Created by Abraham Campbell on 15/01/2020.
 *   Copyright (c) 2020  Abraham Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
   
   (MIT LICENSE ) e.g do what you want with this :-) 
 */
public class Model {
	/**
	 * mode: 0 is single player mode; 1 is duo player mode;
	 */
	private int mode = 0;
	private PlayerGameObject player;
	private PlayerGameObject player2;
//	private GameObject agentAphla;
//	private GameObject agentBeta;
	private int score;
	private BossGameObject bossGameObject;
	private CopyOnWriteArrayList<EnemyGameObject> enemiesList;
	private CopyOnWriteArrayList<EnemyGameObject> tempEnemyList;
	private CopyOnWriteArrayList<GameObject> mapList;
	private CopyOnWriteArrayList<EffectiveGameObject> effectList;
	private CopyOnWriteArrayList<AmmoGameObject> ammoList;

	private CopyOnWriteArrayList<EnemyBulletGameObject> enemyBullets;
	private CopyOnWriteArrayList<BulletGameObject> playerBullets;
	private CopyOnWriteArrayList<UiPanel> uiPanels;

	private int windowPos;
	private int upperbound;
	private int lowerbound;
	private boolean isSettingToggled;
	private boolean startUpdate = false;
	private boolean isTriggeredLost = false;
	private boolean isTriggeredWon = false;
	private boolean isEnd = false;
	private boolean pause = false;
	private boolean freezeKeyboard = false;
	private boolean isBoss;
	private boolean firstBlack = true;
	private boolean isBlack = true;
	private boolean isMute = false;
	private boolean playWon;
	private long endTimestamp = 0;
	private Map map;
	private long lastPauseTime;
	private int gameStatus = Util.PLAYING;
//	private CopyOnWriteArrayList<GameObject> current

	public Model() {
		init();
	}

	private void init() {
		// setup game world
		enemiesList = new CopyOnWriteArrayList<EnemyGameObject>();
		mapList = new CopyOnWriteArrayList<GameObject>();
		effectList = new CopyOnWriteArrayList<EffectiveGameObject>();
		ammoList = new CopyOnWriteArrayList<AmmoGameObject>();

		enemyBullets = new CopyOnWriteArrayList<EnemyBulletGameObject>();
		playerBullets = new CopyOnWriteArrayList<BulletGameObject>();
		uiPanels = new CopyOnWriteArrayList<UiPanel>();
		windowPos = 0;
		upperbound = 850;
		lowerbound = 550;
		startUpdate = false;
		playWon = false;
		isBoss = false;
		endTimestamp = 0;
		// initialize the player
		player = new PlayerGameObject(new Point3f(60, 50, 0));
		if (mode == 1) {
			player2 = new PlayerGameObject(new Point3f(20, 50, 0));
		}
		BulletGameObject.setEffectiveDistance(BulletGameObject.INIT_EFFECTIVE_DISTANCE);
		BulletGameObject.setSize(BulletGameObject.INIT_SIZE);

		// initialize the map (will substitude with JSON loader)
		map = MapLoader.loadFromFile("maps/default-map.json");

//				map = MapLoader.loadFromFile("maps/test.json");
		// load ground and shelter from the map object
		for (int i = 0; i < map.getGroundPos().length; i++) {
			mapList.add(new BasicGroundUnitGameObject(map.getGroundPos()[i], (int) map.getGroundPos()[i].getZ()));
		}

		for (int i = 0; i < map.getShelterPos().length; i++) {
			mapList.add(new ShelterGameObject(map.getShelterPos()[i]));
		}

		for (int i = 0; i < map.getPlantPos().length; i++) {
			mapList.add(new PlantGameObject(map.getPlantPos()[i], (int) map.getPlantPos()[i].getZ()));
		}
		for (int i = 0; i < map.getBoxPos().length; i++) {
			mapList.add(new BoxGameObject(map.getBoxPos()[i], (int) map.getBoxPos()[i].getZ()));
		}

		for (int i = 0; i < map.getMovableEnemyPos().length; i++) {
			EnemyGameObject enemyGameObject = new EnemyGameObject(map.getMovableEnemyPos()[i], true);
			enemiesList.add(enemyGameObject);
		}

		// Initialize the players and agents
	}

	private void initBoss() {
		bossGameObject = new BossGameObject(new Point3f(640, 360, 0), map.getBossHealth(), map.getWaveInterval());
		ammoList = new CopyOnWriteArrayList<AmmoGameObject>();
		enemiesList = new CopyOnWriteArrayList<EnemyGameObject>();
		mapList = new CopyOnWriteArrayList<GameObject>();
		tempEnemyList = new CopyOnWriteArrayList<EnemyGameObject>();
		for (int i = 0; i < 20; i++) {
			tempEnemyList.add(new EnemyGameObject(bossGameObject.getHeadPos(), true));
		}
		windowPos = 0;
		map.setEndX(1280);
		Point3f[] mapPoints = bossGameObject.getCaveGround();
		for (int i = 0; i < mapPoints.length; i++) {
			mapList.add(new CaveGameObject(mapPoints[i]));
		}
		// reset player's pos
		Point3f position = new Point3f(400, 500, 0);
		player.getRigidbody().setPosition(position);
		player.syncPos();
		if (mode == 1) {
			position = new Point3f(350, 500, 0);
			player2.getRigidbody().setPosition(position);
			player2.syncPos();
		}
		if (!isMute && gameStatus == Util.PLAYING) {
			MusicPlayer.getInstance().stopGameBGM();
			MusicPlayer.getInstance().playBossBGM();
			MusicPlayer.getInstance().playBossOut();
		}
		isBlack = false;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public int getMode() {
		return mode;
	}

	// This is the heart of the game , where the model takes in all the inputs
	// ,decides the outcomes and then changes the model accordingly.
	public void gamelogic() {
		if (isEnd) {
			endLogic();
		}
//		if (isSettingToggled()) {
//			if (Controller.getInstance().isESCPressed()) {
//				isSettingToggled = false;
//			}
//		}
		if (isPause()) {
			waitForStart();
		} else {
			controllerLogic();
			// Player Logic first
			playerLogic();
			// boss logic
			bossLogic();
			// Enemy Logic next
			enemyLogic();
			// Bullets move next
			bulletLogic();

			ammoLogic();

			// interactions between objects
			gameLogic();

			effectLogic();

			updateViewingWindow();
		}

	}

	/**
	 * Controller manager is from Maven
	 */
	private void controllerLogic() {
		Controller controller = Controller.getInstance();
		ControllerState currState = controller.getControllerManager().getState(0);
		if (currState.isConnected) {
			ControllerIndex currentController = controller.getControllerManager().getControllerIndex(0);
			try {
				if (currentController.isButtonPressed(ControllerButton.A)) {
					controller.setKeySpacePressed(true);
				} else {
					controller.setKeySpacePressed(false);
				}
				if (currentController.getAxisState(ControllerAxis.TRIGGERRIGHT) >= 0.5f) {
					controller.setKeyJPressed(true);
				} else {
					controller.setKeyJPressed(false);
				}

				float leftX = currentController.getAxisState(ControllerAxis.LEFTX);
				float leftY = currentController.getAxisState(ControllerAxis.LEFTY);

				if (leftX < -0.4f) {
					controller.setKeyAPressed(true);
				} else if (leftX > 0.4f) {
					controller.setKeyDPressed(true);
				} else {
					controller.setKeyAPressed(false);
					controller.setKeyDPressed(false);
				}
				if (leftY < -0.4f) {
					controller.setKeySPressed(true);
				} else if (leftY > 0.4f) {
					controller.setKeyWPressed(true);
				} else {
					controller.setKeyWPressed(false);
					controller.setKeySPressed(false);
				}
//				if (currentController.isButtonPressed(ControllerButton.DPAD_LEFT)) {
//					controller.setKeyAPressed(true);
//				} else {
//					controller.setKeyAPressed(false);
//				}
//				if (currentController.isButtonPressed(ControllerButton.DPAD_RIGHT)) {
//					controller.setKeyDPressed(true);
//				} else {
//					controller.setKeyDPressed(false);
//				}
//				if (currentController.isButtonPressed(ControllerButton.DPAD_DOWN)) {
//					controller.setKeySPressed(true);
//				} else {
//					controller.setKeySPressed(false);
//				}
//				if (currentController.isButtonPressed(ControllerButton.DPAD_UP)) {
//					controller.setKeyWPressed(true);
//				} else {
//					controller.setKeyWPressed(false);
//				}
			} catch (ControllerUnpluggedException e) {

				e.printStackTrace();
			}
		}
	}

	private void waitForStart() {
		if (Controller.getInstance().isKeyPPressed() && Util.getCurrentTime() - lastPauseTime > 10) {
			lastPauseTime = Util.getCurrentTime();
			pause = false;
			if (!isMute) {
				MusicPlayer.getInstance().playPauseSound();
			}
		}
	}

	private void endLogic() {
		if (gameStatus == Util.LOST) {

			if (Controller.getInstance().isKeyJPressed()) {
				isTriggeredLost = false;
				isEnd = false;
				gameStatus = Util.PLAYING;
				init();
			}
			if (!MusicPlayer.getInstance().isBossBGMEnd()) {
				MusicPlayer.getInstance().stopBossBGM();
			}
			MusicPlayer.getInstance().stopGameBGM();
		} else if (gameStatus == Util.WON) {
			if (!MusicPlayer.getInstance().isBossBGMEnd()) {
				MusicPlayer.getInstance().stopBossBGM();
			}
			MusicPlayer.getInstance().stopGameBGM();
		}
	}

	private void gameLogic() {

		// this is a way to increment across the array list data structure

		// see if they hit anything
		// using enhanced for-loop style as it makes it a lot easier both code wise and

		if (Util.getCurrentTime() % 3900 == 0) {
			if (!isMute && !isEnd && !isBoss) {
				MusicPlayer.getInstance().playGameBGM();
			}
		}
		if (gameStatus == Util.LOST && !isTriggeredLost) {
			if (!isMute) {
				if (isBoss) {
					MusicPlayer.getInstance().stopBossBGM();
				} else {
					MusicPlayer.getInstance().stopGameBGM();
					if (isBoss) {
						MusicPlayer.getInstance().stopBossBGM();
					}
				}
				MusicPlayer.getInstance().playLostSound();
			}
			endTimestamp = Util.getCurrentTime();
			isTriggeredLost = true;
		}
		if (gameStatus == Util.WON && !isTriggeredWon) {
			if (!isMute) {
				MusicPlayer.getInstance().stopBossBGM();
				MusicPlayer.getInstance().stopGameBGM();
				MusicPlayer.getInstance().playBossDie();
			}
			endTimestamp = Util.getCurrentTime();
			isTriggeredWon = true;
		}
		if ((isTriggeredLost || isTriggeredWon) && Util.getCurrentTime() - endTimestamp > 240) {
			isEnd = true;
			if (isTriggeredWon && !playWon) {
				playWon = true;
				if (!isMute) {
					MusicPlayer.getInstance().playWinSound();
				}
			}
		}

		if (player.getCentre().getY() > 1000 && player.getStatus() < Util.DEFEATED) {
			player.hurt(isMute);
		}

//		if (mode == 1 && player2.getCentre().getY() > 1000 && player2.getStatus() < Util.DEFEATED) {
//			player2.hurt();
//		}

		if (player.getCentre().getX() > (map.getEndX() - 500) && player.getCentre().getX() < (map.getEndX())
				&& !isBoss) {
			freezeKeyboard = true;
			player.moveForward();
		}

//		if (mode == 1 && player2.getCentre().getX() > (map.getEndX() - 500)
//				&& player2.getCentre().getX() < (map.getEndX()) && !isBoss) {
//			freezeKeyboard = true;
//			player2.moveForward();
//		}

		if (mode == 1 && player2.getCentre().getX() >= (map.getEndX() - 20) && !isBoss) {
			isBlack = true;
		}

		if (player.getCentre().getX() >= (map.getEndX() - 20) && !isBoss) {
			isBlack = true;
		}

		if (player.getCentre().getX() >= (map.getEndX() - 5) && !isBoss) {
			freezeKeyboard = false;
			isBoss = true;
			isBlack = true;
			initBoss();
		}

		if (mode == 1 && player2.getCentre().getX() >= (map.getEndX() - 5) && !isBoss) {
			freezeKeyboard = false;
			isBoss = true;
			isBlack = true;
			initBoss();
		}
	}

	private void bossLogic() {
		if (isBoss) {
			// check if bgm ended
			if ((Util.getCurrentTime() - bossGameObject.getStartTimestamp()) % 3420 == 0) {
				MusicPlayer.getInstance().playBossBGM();
			}
			if (bossGameObject.update(isMute)) {
				switch (bossGameObject.getStatus()) {
				case 1:
					// render laser shooting
					// will be added next version
					break;
				case 2:
					if (Util.getCurrentTime() % bossGameObject.getShottInterval() == 0) {
						createBossBullet(new Point3f(bossGameObject.getCurrentHeadPos().getX(),
								bossGameObject.getCurrentHeadPos().getY() + 50, 0), player.getCentre(), 0);
					}
					break;
				case 3:
					if (!bossGameObject.isMoving() && (Util.getCurrentTime()
							% (bossGameObject.getShottInterval() * 5) == 0
							|| Util.getCurrentTime()
									% (bossGameObject.getShottInterval() * 5) == bossGameObject.getShottInterval() / 3
							|| Util.getCurrentTime()
									% (bossGameObject.getShottInterval() * 5) == bossGameObject.getShottInterval() * 2
											/ 3)) {
						EnemyGameObject temp = tempEnemyList.get(0);
						enemiesList.add(temp);
						tempEnemyList.remove(temp);
						if (!isMute) {
							MusicPlayer.getInstance().playGenerateSound();
						}
					}
					break;
				default:
					break;
				}
			} else {
				// end
				gameStatus = Util.WON;
			}
		}
	}

	private void updateViewingWindow() {
		if (map.getEndX() > (windowPos + upperbound) && player.getCentre().getX() >= (windowPos + lowerbound)) {
			startUpdate = true;
		}

//		if (mode == 1 && map.getEndX() > (windowPos + upperbound)
//				&& player2.getCentre().getX() >= (windowPos + lowerbound)) {
//			startUpdate = true;
//		}

		if (map.getEndX() <= (windowPos + 1280) && !isBoss) {
			startUpdate = false;
		}

		if (startUpdate && !isBoss) {
			windowPos += player.getSpeedOfMovement();
			if (player.getCentre().getX() <= (windowPos + lowerbound)) {
				startUpdate = false;
			}
//			if (mode == 2 && player2.getCentre().getX() <= (windowPos + lowerbound)) {
//				startUpdate = false;
//			}
		}
	}

	private void ammoLogic() {
		for (AmmoGameObject ammoGameObject : ammoList) {
			SimpleRectRigidbody ammoRigidbody = (SimpleRectRigidbody) ammoGameObject.getRigidbody();
			ammoGameObject.initFrame();
			for (GameObject gameObject : mapList) {
				if (gameObject.getRigidbody().isCollidedWith(ammoGameObject.getRigidbody())) {
					if ((gameObject.getRigidbody().position().getY() - ammoRigidbody.position()
							.getY()) >= ((ammoRigidbody.getRigidHeight()
									+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidHeight()) / 2)
									+ player.getMaxFallingSpeed() - 1) {
						ammoGameObject.setGround(true);
						Point3f temPoint3f = new Point3f(ammoRigidbody.position().getX(),
								(gameObject.getRigidbody().position().getY() - (ammoRigidbody.getRigidHeight()
										+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidHeight()) / 2),
								0);
						ammoRigidbody.setPosition(temPoint3f);
						ammoGameObject.syncPos();
					}
				}
			}
			if (!ammoGameObject.isGround()) {
				if (ammoGameObject.getCurrentSpeed() > player.getMaxFallingSpeed()) {
					ammoGameObject.setCurrentSpeed(ammoGameObject.getCurrentSpeed() - Util.g * 0.1f);
				} else {
					ammoGameObject.setCurrentSpeed(player.getMaxFallingSpeed());
				}
				Point3f temPoint3f = ammoGameObject.getRigidbody().position();
				temPoint3f.ApplyVector(new Vector3f(0, ammoGameObject.getCurrentSpeed(), 0));
				ammoGameObject.getRigidbody().setPosition(temPoint3f);
				ammoGameObject.syncPos();
			}
			if (ammoGameObject.getRigidbody().isCollidedWith(player.getRigidbody())) {
				UiPanel uiPanel = null;
				switch (ammoGameObject.getAmmoType()) {
				case 1:
					BulletGameObject.setEffectiveDistance(BulletGameObject.getEffectiveDistance() + 100);
					uiPanel = new UiPanel(350, 80, UiFont.MEDIUM, 120, "ammo range up", false);
					break;
				case 2:
					BulletGameObject.setSize(BulletGameObject.getSize() + 4);
					uiPanel = new UiPanel(350, 80, UiFont.MEDIUM, 120, "ammo size up", false);
					break;
				case 3:
					player.setShootInterval(player.getShootInterval() - 5);
					uiPanel = new UiPanel(350, 80, UiFont.MEDIUM, 120, "ammo rate up", false);
					break;
				default:
					BulletGameObject.setEffectiveDistance(BulletGameObject.getEffectiveDistance() + 100);
					uiPanel = new UiPanel(350, 80, UiFont.MEDIUM, 120, "ammo range up", false);
					break;
				}
				uiPanels.add(uiPanel);
				if (!isMute) {
					MusicPlayer.getInstance().playAmmoSound();
				}
				ammoList.remove(ammoGameObject);
			}

		}
	}

	private void enemyLogic() {

		for (EnemyGameObject temp : enemiesList) {
			temp.initFrame();
			SimpleRectRigidbody enemyRigidbody = (SimpleRectRigidbody) temp.getRigidbody();
			// Move enemies
			if (temp.getRigidbody().isCollidedWith(player.getRigidbody())) {
				if (!player.isUnbreakable() && player.getStatus() < Util.HURT && !temp.isDead()) {
					player.hurt(isMute);
				}
			}
			if (temp.isDead() && (Util.getCurrentTime() - temp.getDieTime()) >= 99) {
				enemiesList.remove(temp);
			}
			if (temp.getCentre().getY() > 1300) {
				enemiesList.remove(temp);
			}
			for (GameObject gameObject : mapList) {
				if (gameObject.getRigidbody().isCollidedWith(temp.getRigidbody())) {
					if ((gameObject.getRigidbody().position().getY() - enemyRigidbody.position()
							.getY()) >= ((enemyRigidbody.getRigidHeight()
									+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidHeight()) / 2)
									+ player.getMaxFallingSpeed() - 1) {
						temp.setGround(true);
						Point3f temPoint3f = new Point3f(enemyRigidbody.position().getX(),
								(gameObject.getRigidbody().position().getY() - (enemyRigidbody.getRigidHeight()
										+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidHeight()) / 2),
								0);
						enemyRigidbody.setPosition(temPoint3f);
						temp.setCurrentJumpSpeed(0);
						temp.syncPos();
						temp.setJumping(false);
					} else {
						// ground is on the right
						if (Math.abs(
								gameObject.getRigidbody().position().getX() - enemyRigidbody.position().getX()) > -5
										+ (enemyRigidbody.getRigidWidth()
												+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidWidth())
												/ 2) {
							enemyRigidbody.freezeX();
						}
						// ground is on the left
						else if (Math.abs(
								gameObject.getRigidbody().position().getX() - enemyRigidbody.position().getX()) < -5
										+ (enemyRigidbody.getRigidWidth()
												+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidWidth())
												/ 2) {
							enemyRigidbody.freezeY();
						}
					}
				}
			}
			if (!temp.isGround()) {
				float currentJumpSpeed = temp.getCurrentJumpSpeed();
				currentJumpSpeed -= Util.g * enemyRigidbody.mass() * 0.1f;
				if (currentJumpSpeed < player.getMaxFallingSpeed()) {
					currentJumpSpeed = player.getMaxFallingSpeed();
				}
				temp.setCurrentJumpSpeed(currentJumpSpeed);
				Point3f temPoint3f = enemyRigidbody.position();
				temPoint3f.ApplyVector(new Vector3f(0, currentJumpSpeed, 0));
				enemyRigidbody.setPosition(temPoint3f);
				temp.syncPos();
			}
			if (!temp.isDead()) {
				Object[] objects = new Object[2];
				objects[0] = player;
				objects[1] = temp;
				temp.getAgent().sense(objects);

				// then AI agent move the agent:
				int plan = temp.getAgent().producePlan();
				// decode the plan
				if (plan / EnemyAI.SHOOT_RIGHT > 0) {
					temp.setHorizontalDirection(Util.FORWARD);
					if (temp.shoot()) {
						createEnemyBullet(temp);
					}
					plan -= EnemyAI.SHOOT_RIGHT;
				}
				if (plan / EnemyAI.SHOOT_LEFT > 0) {
					temp.setHorizontalDirection(Util.BACK);
					if (temp.shoot()) {
						createEnemyBullet(temp);
					}
					plan -= EnemyAI.SHOOT_LEFT;
				}
				if (plan / EnemyAI.JUMP > 0) {
					temp.jump();
					plan -= EnemyAI.JUMP;
				}
				if (plan / EnemyAI.MOVE_BACKWARD > 0) {
					temp.moveBackward();
					plan -= EnemyAI.MOVE_BACKWARD;
				}
				if (plan / EnemyAI.MOVE_FORWARD > 0) {
					temp.moveForward();
					plan -= EnemyAI.MOVE_FORWARD;
				}
			}
		}

	}

	private void effectLogic() {

		for (GameObject gameObject : effectList) {
			Animation animation = gameObject.getAnimation();
			int currentFrame = (int) (((Util.getCurrentTime() - ((EffectiveGameObject) gameObject).getStartTime())
					% animation.getDuration()) * animation.getNumOfFrame() / animation.getDuration());
			if (currentFrame == animation.getNumOfFrame() - 1) {
				effectList.remove(gameObject);
			}
		}
	}

	private void bulletLogic() {
		// move bullets

		for (BulletGameObject temp : playerBullets) {
			// the bullet blasts when collide with ground
			for (GameObject gameObject : mapList) {
				if (gameObject.getRigidbody().isCollidedWith(temp.getRigidbody())) {
					effectList.add(new EffectiveGameObject(temp.getCentre(), EffectiveGameObject.BULLET_EFFECT));
					playerBullets.remove(temp);
					if (gameObject instanceof BoxGameObject) {
						BoxGameObject tempBoxGameObject = (BoxGameObject) gameObject;
						if (tempBoxGameObject.isExploded()) {
							AmmoGameObject ammoGameObject = new AmmoGameObject(
									new Point3f(tempBoxGameObject.getCentre().getX(),
											tempBoxGameObject.getCentre().getY() - 50, 0),
									tempBoxGameObject.getType());
							ammoList.add(ammoGameObject);
							effectList.add(new EffectiveGameObject(tempBoxGameObject.getCentre(),
									EffectiveGameObject.EXPLOSION_RED));
							mapList.remove(tempBoxGameObject);
							if (!isMute) {
								MusicPlayer.getInstance().playBoxExplosionSound();
							}
						} else {
							tempBoxGameObject.hit();
						}
					}
				}
			}
			boolean shotStatus = temp.shoot();
			if (!shotStatus) {
				playerBullets.remove(temp);
			}
			for (EnemyGameObject enemy : enemiesList) {
				if (!enemy.isDead() && temp.getRigidbody().isCollidedWith(enemy.getRigidbody())) {
					enemy.setDead();
					if (!isMute) {
						MusicPlayer.getInstance().playEnemyDieSound();
					}
					effectList.add(new EffectiveGameObject(temp.getCentre(), EffectiveGameObject.BULLET_EFFECT));
					playerBullets.remove(temp);
				}
			}
			if (isBoss && !bossGameObject.isMoving()) {
				if (bossGameObject.getRigidbody().isCollidedWith(temp.getRigidbody())) {
					effectList.add(new EffectiveGameObject(temp.getCentre(), EffectiveGameObject.BULLET_EFFECT));
					playerBullets.remove(temp);
					bossGameObject.hit(isMute);
				}
			}
		}
		for (EnemyBulletGameObject bulletGameObject : enemyBullets) {
			if (bulletGameObject.getRigidbody().isCollidedWith(player.getRigidbody())) {
				if (!player.isUnbreakable() && player.getStatus() < Util.HURT) {
					player.hurt(isMute);
				}
			}
			if (!bulletGameObject.shoot()) {
				enemyBullets.remove(bulletGameObject);
			}
			for (GameObject gameObject : mapList) {
				if (gameObject.getRigidbody().isCollidedWith(bulletGameObject.getRigidbody())) {
					enemyBullets.remove(bulletGameObject);
				}
			}
		}

	}

	private void playerLogic() {

		// reset all player status to default
		player.initFrame();

		// smoother animation is possible if we make a target position // done but may
		// try to change things for students

		if (player.getStatus() == Util.SOUL) {
			gameStatus = Util.LOST;
			return;
		}

		// check for movement and if you fired a bullet

		SimpleRectRigidbody playRigidbody = (SimpleRectRigidbody) player.getRigidbody();

		// Apply gravity to players & check collision vertically.
		if (playRigidbody.getGravityStatus()) {
			for (GameObject gameObject : mapList) {
				if (gameObject.getRigidbody().isCollidedWith(playRigidbody)) {
					if (player.getCurrentJumpSpeed() <= 0
							&& (gameObject.getRigidbody().position().getY() - playRigidbody.position()
									.getY()) >= ((playRigidbody.getRigidHeight()
											+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidHeight()) / 2)
											+ player.getMaxFallingSpeed() - 1) {
						if (firstBlack) {
							firstBlack = false;
							isBlack = false;
						}
						player.setOnGround();
						Point3f temPoint3f = new Point3f(playRigidbody.position().getX(),
								(gameObject.getRigidbody().position().getY() - (playRigidbody.getRigidHeight()
										+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidHeight()) / 2),
								0);
						playRigidbody.setPosition(temPoint3f);
						player.setJumping(false);
						player.setDoubleJump(false);
						player.setCurrentJumpSpeed(0);
						player.syncPos();
					} else {
						// ground is on the right
						if ((gameObject.getRigidbody().position().getX() - playRigidbody.position().getX()) > -3
								+ (playRigidbody.getRigidWidth()
										+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidWidth()) / 2) {
							playRigidbody.freezeX();
						}
						// ground is on the left
						else if ((playRigidbody.position().getX() - gameObject.getRigidbody().position().getX()) > -3
								+ (playRigidbody.getRigidWidth()
										+ ((SimpleRectRigidbody) gameObject.getRigidbody()).getRigidWidth()) / 2) {
							playRigidbody.freezeY();
						}
					}
				}

			}
			if (!player.isGround()) {
				float currentJumpSpeed = player.getCurrentJumpSpeed();
				currentJumpSpeed -= Util.g * playRigidbody.mass() * 0.1f;
				Point3f temPoint3f = playRigidbody.position();
				player.setCurrentJumpSpeed(currentJumpSpeed);
				temPoint3f.ApplyVector(new Vector3f(0, player.getCurrentJumpSpeed(), 0));
				playRigidbody.setPosition(temPoint3f);
				player.syncPos();
			}
		}

		// Freeze player if not kinematic
		if (!player.getRigidbody().isKinematic()) {
			return;
		}

		if (Controller.getInstance().isKeyAPressed() && (player.getCentre().getX() - windowPos) > 20
				&& !freezeKeyboard) {
			player.moveBackward();
			if (Util.getCurrentTime() % 30 == 0) {
				createDust(player.getCentre(), Util.BACK);
			}
		}

		if (Controller.getInstance().isKeyDPressed() && (player.getCentre().getX() - windowPos) < 1260
				&& !freezeKeyboard) {
			player.moveForward();
			if (Util.getCurrentTime() % 30 == 0) {
				createDust(player.getCentre(), Util.FORWARD);
			}
		}

		if (Controller.getInstance().isKeyWPressed() && !freezeKeyboard) {
			player.lookUp();
		}

		if ((Controller.getInstance().isKeySPressed()) && !freezeKeyboard) {
			player.lookDown();
		}

		if (Controller.getInstance().isKeyPPressed() && Util.getCurrentTime() - lastPauseTime > 10 && !freezeKeyboard) {
			lastPauseTime = Util.getCurrentTime();
			pause = true;
			if (!isMute) {
				MusicPlayer.getInstance().playPauseSound();
			}
		}

		if (Controller.getInstance().isKeyJPressed() && !freezeKeyboard) {
			player.shot();
			if (Util.getCurrentTime() - player.getLastShotTimestamp() > player.getShootInterval()) {
				createBullet();
				if (!isMute) {
					MusicPlayer.getInstance().playFlameGunEffect();
				}
				player.setLastShotTimestamp(Util.getCurrentTime());
			}
		}
		if ((Controller.getInstance().isKeySpacePressed() || Controller.getInstance().isKeyKPressed())
				&& !freezeKeyboard) {
			player.jump(isMute);
			Controller.getInstance().setKeySpacePressed(false);
		}

	}

	private void createDust(Point3f center, int direction) {
		Point3f dustCenter = new Point3f(center.getX(), (center.getY() + player.getHeight() / 2) - 20, 0);
		EffectiveGameObject gameObject = new EffectiveGameObject(dustCenter, EffectiveGameObject.RUN_DUST);
		gameObject.setDirection(direction);
		effectList.add(gameObject);
	}

	private void createEnemyBullet(EnemyGameObject enemy) {
		float x = enemy.getCentre().getX();
		float y = enemy.getCentre().getY();
		Vector3f shotDirection = null;
		Point3f startPos = null;
		if (enemy.getHorizontalDirection() == Util.FORWARD) {
			shotDirection = new Vector3f(1, 0, 0);
			startPos = new Point3f(x + 10, y + 10, 0);
		} else {
			shotDirection = new Vector3f(-1, 0, 0);
			startPos = new Point3f(x - 10, y + 10, 0);
		}
		EnemyBulletGameObject enemyBulletGameObject = new EnemyBulletGameObject(startPos, shotDirection);
		enemyBullets.add(enemyBulletGameObject);
	}

	private void createBossBullet(Point3f center, Point3f towards, int bias) {
		Vector3f shotDirection = null;
		Vector3f vector = center.MinusPoint(towards);
		for (int i = -2; i < 2; i++) {
			shotDirection = new Vector3f(-vector.getX() - i * 60, vector.getY() - i * 60, 0);
			EnemyBulletGameObject enemyBulletGameObject = new EnemyBulletGameObject(center, shotDirection, true);
			enemyBullets.add(enemyBulletGameObject);
		}
		if (!isMute) {
			MusicPlayer.getInstance().playBossShootSound();
		}
	}

	private void createBullet() {
		// initialize a bullet and add it into list
		float playerX = player.getCentre().getX();
		float playerY = player.getCentre().getY();
		int width = player.getWidth();
		int height = player.getHeight();
		int bias = 10;
		Vector3f shotDirection = null;
		Point3f startPos = null;
		switch (player.getVerticalDirection()) {
		case Util.UP:
			if (player.getStatus() == Util.RUN) {
				if (player.getHorizontalDirection() == Util.FORWARD) {
					// bullet generate from right top corner
					shotDirection = new Vector3f(1, 1, 0);
					startPos = new Point3f(playerX + width / 2, playerY - height / 2, 0);
				} else {
					// bullet generatefrom left top corner
					shotDirection = new Vector3f(-1, 1, 0);
					startPos = new Point3f(playerX - width / 2, playerY - height / 2, 0);
				}
			} else {
				// bullet from top center
				shotDirection = new Vector3f(0, 1, 0);
				startPos = new Point3f(playerX, playerY - height / 2, 0);
			}
			break;
		case Util.DOWN:
			if (player.getHorizontalDirection() == Util.FORWARD) {
				// right bottum
				shotDirection = new Vector3f(1, -1, 0);
				startPos = new Point3f(playerX + width / 2, playerY + height / 2, 0);
			} else {
				// left bottum
				shotDirection = new Vector3f(-1, -1, 0);
				startPos = new Point3f(playerX - width / 2, playerY + height / 2, 0);
			}
			break;
		default:
			if (player.getHorizontalDirection() == Util.FORWARD) {
				// right center
				shotDirection = new Vector3f(1, 0, 0);
				startPos = new Point3f(playerX + width / 2, playerY + bias, 0);
			} else {
				// left center
				shotDirection = new Vector3f(-1, 0, 0);
				startPos = new Point3f(playerX - width / 2, playerY + bias, 0);
			}
			break;
		}
		BulletGameObject bulletGameObject = new BulletGameObject(startPos, shotDirection);
		bulletGameObject.getAnimation().setAnimationStartTime(Util.getCurrentTime());
		playerBullets.add(bulletGameObject);
	}

	public int getWindowPos() {
		return windowPos;
	}

	public PlayerGameObject getPlayerGameObject() {
		return player;
	}

	public CopyOnWriteArrayList<EffectiveGameObject> getEffectList() {
		return effectList;
	}

	public CopyOnWriteArrayList<EnemyBulletGameObject> getEnemyBullets() {
		return enemyBullets;
	}

	public CopyOnWriteArrayList<GameObject> getMapList() {
		return mapList;
	}

	public CopyOnWriteArrayList<EnemyGameObject> getEnemies() {
		return enemiesList;
	}

	public CopyOnWriteArrayList<BulletGameObject> getPlayerBullets() {
		return playerBullets;
	}

	public void setGameStatus(int gameStatus) {
		this.gameStatus = gameStatus;
	}

	public int getGameStatus() {
		return gameStatus;
	}

	public CopyOnWriteArrayList<UiPanel> getUiPanels() {
		return uiPanels;
	}

	public void removePanel(UiPanel uiPanel) {
		uiPanels.remove(uiPanel);
	}

	public CopyOnWriteArrayList<AmmoGameObject> getAmmoList() {
		return ammoList;
	}

	public Map getMap() {
		return map;
	}

	public boolean isEnd() {
		return isEnd;
	}

	public boolean isTriggeredLost() {
		return isTriggeredLost;
	}

	public boolean isBlack() {
		return isBlack;
	}

	public void setBlack(boolean isBlack) {
		this.isBlack = isBlack;
	}

	public boolean isTriggeredWon() {
		return isTriggeredWon;
	}

	public boolean isPause() {
		return pause;
	}

	public boolean isBoss() {
		return isBoss;
	}

	public BossGameObject getBossGameObject() {
		return bossGameObject;
	}

	public boolean isMute() {
		return isMute;
	}

	public void setMute(boolean isMute) {
		this.isMute = isMute;
	}

	public boolean isSettingToggled() {
		return isSettingToggled;
	}

	public void setSettingToggled(boolean isSettingToggled) {
		this.isSettingToggled = isSettingToggled;
	}
}

/*
 * MODEL OF your GAME world
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWWNNNXXXKKK000000000000KKKXXXNNNWWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWNXXK0OOkkxddddooooooolllllllloooooooddddxkkOO0KXXNWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWNXK0OkxddooolllllllllllllllllllllllllllllllllllllllloooddxkO0KXNWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNXK0OkdooollllllllooddddxxxkkkOOOOOOOOOOOOOOOkkxxdddooolllllllllllooddxO0KXNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNK0kxdoollllllloddxkO0KKXNNNNWWWWWWMMMMMMMMMMMMMWWWWNNNXXK00Okkxdoollllllllloodxk0KNWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXKOxdooolllllodxkO0KXNWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWNXK0OkxdolllllolloodxOKXWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKOxdoolllllodxO0KNWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNXKOkdolllllllloodxOKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0kdolllllooxk0KNWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNK0kdolllllllllodk0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0xdolllllodk0XNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWWMMMMMMMMMMMWN0kdolllllllllodx0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0xoollllodxOKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWWMMMMMMMMMMWNXKOkkkk0WMMMMMMMMMMMMWNKkdolllloololodx0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWN0kdolllllox0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNXK0kxk0KNWWWWNX0OkdoolllooONMMMMMMMMMMMMMMMWXOxolllllllollodk0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXOdollllllllokXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWN0xooollloodkOOkdoollllllllloxXWMMMMMMMMMMMMMMMWXkolllllllllllllodOXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWN0koolllllllllllokNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKxolllllllllllllllllllllllllllox0XWWMMMMMMMMMWNKOdoloooollllllllllllok0NWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0xoolllllllllllllloONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKxllolllllllllllllllllloollllllolodxO0KXNNNXK0kdoooxO0K0Odolllollllllllox0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMWXOdolllllllllllllllllokXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNkolllllllllloolllllllllllllllllllolllloddddoolloxOKNWMMMWNKOxdolollllllllodOXWMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMWXOdolllolllllllllllllloxKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXxlllolllllloxkxolllllllllllllllllolllllllllllllxKWMWWWNNXXXKKOxoollllllllllodOXWMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMWXOdollllllllllllllllllllokNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNOollllllllllxKNKOxooollolllllllllllllllllllolod0XX0OkxdddoooodoollollllllllllodOXWMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMN0xollllllllllllllllllllllld0NMMMMMMMMMMMMMMMMMMMMMMMWWNKKNMMMMMMMMMMMW0dlllllllllokXWMWNKkoloolllllllllllllllllllolokkxoolllllllllllllollllllllllllllox0NMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMWKxolllllllllllllllllllllllllloONMMMMMMMMMMMMMMMMMMMWNKOxdookNMMMMMMMMMWXkollllllodx0NWMMWWXkolooollllllllllllllllllllooollllllllllllllolllllllllllloooolloxKWMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMWXOdllllllllllllllooollllllllollld0WMMMMMMMMMMMMMMMMWXOxollllloOWMMMMMMMWNkollloodxk0KKXXK0OkdoollllllllllllllllllllllllllllllollllllllloollllllollllllllllllldOXWMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMN0xolllllllllllolllllllllllloodddddONMMMMMMMMMMMMMMMNOdolllllllokNMMMMMMWNkolllloddddddoooolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllox0NMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMWXkolllllllllllllllllllodxxkkO0KXNNXXXWMMMMMMMMMMMMMMNkolllllllllod0NMMMMMNOollllloollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllolllllllllllllokXWMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMWKxollllllllllllllllllox0NWWWWWMMMMMMMMMMMMMMMMMMMMMMW0dlllllllllllookKNWWNOolollloolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloxKWMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMN0dlllllllllllllllllllldKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMNkoloolllollllolloxO0Odllllllllllllllllllllllllllllllllllllllllllllollllllllllllllllllllllllllllllllllllllllllllllld0NWMMMMMMMMMMMMM
 * MMMMMMMMMMMMMXkolllllllllllllllllolllxXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXOO0KKOdollllllllllooolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloONWMMMMMMMMMMMM
 * MMMMMMMMMMMWXkollllllllllllllllllllllxXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWMMMMWNKOxoollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllokXWMMMMMMMMMMM
 * MMMMMMMMMMWKxollllllllllllllllllllllokNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWKxollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloxKWMMMMMMMMMM
 * MMMMMMMMMWKxollllllllllllodxkkkkkkkO0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMNKOkO0KK0OkdolllllloolllllllllllloooollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloxKWMMMMMMMMM
 * MMMMMMMMWKxllllllllllolodOXWWWWWWWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXxolloooollllllllllllllllloollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllxKWMMMMMMMM
 * MMMMMMMWKxlllllllllollokXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXxololllllllooolloollllloolloooolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllxKWMMMMMMM
 * MMMMMMWXxllllllllooodkKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMKdloollllllllllololodxxddddk0KK0kxxxdollolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllxXWMMMMMM
 * MMMMMMXkolllllodk0KXNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMKdllollllllllllllodOXWWNXXNWMMMMWWWNX0xolollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllokNMMMMMM
 * MMMMMNOollllodONWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dooollllllllllllodOXNWWWWWWMMMMMMMMMWXOddxxddolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloONMMMMM
 * MMMMW0dllllodKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKKK0kdlllllllllllloodxxxxkkOOKNWMMMMMMWNNNNNXKOkdooooollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllld0WMMMM
 * MMMWKxllllloOWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNkolllllollllllllllllllllodOKXWMMMMMMMMMMMMWNXKK0OOkkkxdooolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllxKWMMM
 * MMMNkollllokXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWXOdlllllolllllllllllloloolllooxKWMMMMMMMMMMMMMMMMMMMMWWWNXKOxoollllllllllllllllllllllllllllllllllllllllllllllllllllllllllolokNMMM
 * MMW0ollllldKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKOOkxdollllllllllllllllllllllllllllox0NWMMMMWWNNXXKKXNWMMMMMMMMMWNKOxolllolllllllllllllllllllllllllllllllllllllllllllllllllllllllo0WMM
 * MMXxllllloONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXkolllllllllllllllllllllllllllllllllllooxO000OkxdddoooodkKWMMMMMMMMMMWXxllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllxXWM
 * MWOollllldKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXkollllllllllllllllllllllllllllllllllllllllllllllllllllllld0WMMMMMMMMMWKdlllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloOWM
 * MXxllllloONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXkollllllllllllllllllllllllllllllllllooollllllllllllllllllold0WMMMMMMWN0dolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllxXM
 * W0ollllld0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNKkdolllllllllllllllllllllllllllllllllllllllllllllllllolllllllllokKXNWWNKkollllllllloxdollllllllolllllllllllllllllllllllllolllllllllllllolo0W
 * NkllllloxXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNkollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllodxkkdoolollllllllxKOolllllllllllllllllllllllollooollllllloolllllloolllllkN
 * KxllllloONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0doolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllkX0dlllllllllllllllllllloollloOKKOkxdddoollllllllllllllxK
 * Oolllllo0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXxllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllxXXkollllooolllllllllllllllloONMMMWNNNXX0xolllllllllolloO
 * kolllllo0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNOollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllxXWXkollollllllllllllllllllodKMMMMMMMMMMWKxollllolollolok
 * kllllllo0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dlllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloolllllllllxXWWXkolllllllllllllllolllloONMMMMMMMMMMMW0dllllllllllllk
 * xollolld0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXxllllolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloollllllolloONMMN0xoolllllllolllllllloxXWMMMMMMMMMMMMXxollllllloollx
 * dollllld0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXxlllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloollld0WMMWWXOdollollollllllloxXWMMMMMMMMMMMMMNOollllllokkold
 * olllllld0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNxlllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllldONMMMMWXxollllolllllox0NWMMMMMMMMMMMMMMNOollllllxXOolo
 * llllllld0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXxllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloONMMMMMXxddxxxxkkO0XWMMMMMMMMMMMMMMMMMNOolllllxKW0olo
 * llllllld0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKdlllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllldONWMMMWNXNNWWWMMMMMMMMMMMMMMMMMMMMMMMW0dllollOWW0oll
 * llllllld0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNOolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloxO0KXXXXKKKXNWMMMMMMMMMMMMMMMMMMMMMMMNOdolllkNWOolo
 * ollllllo0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllooooddooloodkKWMMMMMMMMMMMMMMMMMMMMMMWXOolldKNOooo
 * dollllloONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKxolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllloollllo0WMMMMMMMMMMMMMMMMMMMMMMMMXkold0Nkold
 * xollllloxXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXxllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllollokNMMMMMMMMMMMMMMMMMMMMMMMMMWOookXXxolx
 * xolllllloONWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKxolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllokXWMMMMMMMMMMMMMMMMMMMMMMMMMN00XW0dlox
 * kollllllloxOKXXNNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXOxollllllllllllllllllllllllllllllolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllolllllolo0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWOollk
 * OolllllllllloodddkKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKOkkxddooooollllllllllooodxxdollolllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllokXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNkoloO
 * KdllllllllllllllllxXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWNXXXK0OOkkkkkkkkOKXXXNNX0xolllllllllllllllllllllllllllllllllllllllllllllllllllllllloollllllllloox0NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMKdlldK
 * NkllllollloolllllldKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWWWWMMMMMMMMMWNOdlllllllllllllllllllllllllllllllllllllllllllllllllllllllollllllllodOKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWOolokN
 * WOolllllllllllolllokXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKxollllllllllllllllllllllllllllllllllllllllllllllllllllllllllllod0NWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXxolo0W
 * WXxllllllllllllllllox0NWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKxollllllllllllllllllllllllllllllllllllllllllllllllllllllllllokXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNOollxXM
 * MWOollllllllllllllooloxKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKdllllllllllllllllllllllllllllllllllllllllllllllllllllloolld0NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKdlloOWM
 * MWXxllolllllllllllllllldOXWWNNK00KXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dllllllllllllllllllllllllllllllllllllllllllllllllllllllod0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKxollxXWM
 * MMWOollllllllloollllllolodxkxdollodk0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNOollllllllllllllllllllllllllllllllllllllllllllllllllodxOXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWN0dlllo0WMM
 * MMMXxllolllllllllllllllllllllllllllloox0NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN0dooollllllllllllllllllllllllllllllllllllllllllllodOXNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKOkxxolllokNMMM
 * MMMW0dlllllllllllllllllllolllllllollollokXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXOdoolllllllllllllllllllllllllllllllllllllllllllxKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNOoollllllldKWMMM
 * MMMMNOollllllllllllllllllllllllllllllllloOWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXKOdolllllllllllllllllllllllllllllllllllllllloONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNOolllllllloOWMMMM
 * MMMMMXkollllllllllllllllllllllllllllllllokNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dlllllllllllllllllllllllllllllllllllllllld0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dllolllllokNMMMMM
 * MMMMMWXxlllllllllllllllllllllllllllllllloxXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0ollllllllllllllllllllllllllllllllllllllldKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWOollllllllxXWMMMMM
 * MMMMMMWKdlllllllllllllllllllllllllllllllokNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXxolllllllllllllllllllllllllllllllllllllloONWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNOolllllllxKWMMMMMM
 * MMMMMMMW0dlllllllllllllllllllllllllllllloOWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dlllloollllllllllllllllllllllllllllllllloxkOKKXXKKXNMMMMMMMMMMMMMMMMMMMMMMMMNOolllllldKWMMMMMMM
 * MMMMMMMMW0dllllllllllllllllllllllllllllldKMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKdlllllllllllllllllllllllllllllllllllllllllllloooood0WMMMMMMMMMMMMMMMMMMMMMMMNOollolldKWMMMMMMMM
 * MMMMMMMMMW0dlllllllllllllllllllllllllllokXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dllllllllllllllllllllllllllllllolllllllllllllllllld0WMMMMMMMMMMMMMMMMMMMMMMWKxllllldKWMMMMMMMMM
 * MMMMMMMMMMW0dlllllllllllllllllllllllllloxXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNkolllllllllllllllllllllllllllllllllllllllllllllllllxXMMMMMMMMMMMMMMMMMMMMMWXOdolllldKWMMMMMMMMMM
 * MMMMMMMMMMMWKxollllllllllllllllllllllllloOWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dlllllllllllllllllllllllllllllllloolllllllolllollllkNMMMMMMMMMMMMMMMMMMMWXOdolllloxKWMMMMMMMMMMM
 * MMMMMMMMMMMMWKxollllllllllllllllllllllllod0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNkoloollllllllllllllllllllllllllllloddollllllllllllld0WMMMMMMMMMMMMMMMWWNKOdolllllokXWMMMMMMMMMMMM
 * MMMMMMMMMMMMMWXkollllllllllllllllllllllllldKMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dllollllllllllllllllllllllllllllld0XOollllllllllllkNMMMMMMMMMMMMWNK0OkxollllllloONWMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMNOdlllllllllllllllllllllllokXMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN0dlllllllllllllllllllllllllolllld0NWN0dlllllloodxkKWMMMMMMMMMMMMNOollllllllllld0NMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMWKxolollllllllllllllllllokXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNOolllllllllllllllllllllllllllldONMMMWKkdoooxOXNNWMMMMMMMMMMMMMNOollllllllllokXWMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMWXOdlllllllllllllllllloONWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKdllllllllllllllllllllllllllld0NMMMMMWWXXXXNWMMMMMMMMMMMMMMMMW0dlllllllllod0NMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMWKxollolllllllllllloONMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMXxllllllllllllllllllllllllloxKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dlllllllllokXWMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMWNOdollllllloolllldKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNkolllloollllooolllllllllodONWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNkllllllolox0NMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMWXkollllllolllllox0NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKdlllllollllllllllllllodkKWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMW0dllllllodOXWMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMWKxoolllllllllllokXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN0dollllllllllooddxxk0KNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXOdollllldOXWMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMN0xolllllllllllokXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKxolllllodk0KXNNWWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKkdollolodkXWMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMNKxoolllllllllodOKNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWXOdolldOXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0xoollllodkXWMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNKkolllollllllloxOXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKOx0WMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKOdolllllldOXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWKOdollllllllllodx0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKOdoollllloxOXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0xollollollollodxOXNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0kdooollllodk0NWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKkdooolllllllllooxOKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKOxdollllllloxOXWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWN0kdllllllllollllodkOKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWXKOkdoolllllloodOKNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0kdolllllllllllllodxO0XNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNX0OxdollloolllloxOKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWX0kdoolllllllllllllooxkO0XNWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWNX0OkxoololllllllooxOKNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNK0kdoolllllllllllloooodxkO0KXNWWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWNXK0Okxdoolllllollllloxk0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNKOkdoollllllllloolllllloodxkkO00KXXNNWWWWWWMMMMMMMMMWWWWWWWNNXXK00Okxxdoolllllllllllloooxk0KNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNK0kxdoollllllllllllllllllllloodddxxxkkOOOOOOOOOOOkkkxxxdddoollllllllllllllllloodxO0XNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNXK0OxdooollllllllllllooolllllllllllllllllllllllllllllllllllllllllllooodkO0KNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNXK0OkxdooollllllllllllllllllllllllllllllllllllllllllloooddxkO0KXNWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNNXK0OOkkxdddoooooollllllllllllllllooooooddxxkOO0KKXNWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWWNNXXXKK00OOOOOOOOOOOOOOOO00KKXXXNNWWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
 */
