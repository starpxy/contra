package com.starpxy.gameobjects;

import com.starpxy.util.Animation;
import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Util;

public class EffectiveGameObject extends GameObject {
	public static final int BULLET_EFFECT = 5;
	public static final int RUN_DUST = 6;
	public static final int EXPLOSION_RED = 10;
	public static final int EXPLOSION_SMOKE = 12;
	private Animation effectiveAnimation;
	private long startTime;
	private int direction = Util.FORWARD;

	public EffectiveGameObject(Point3f center, int effectNum) {
		setCentre(center);
		startTime = Util.getCurrentTime();
		switch (effectNum) {
		case BULLET_EFFECT:
			effectiveAnimation = new Texture("res/SpriteSheets/fx/bullet_impact.png", 0, 0, 20, 20)
					.loadAnimation(effectNum);
			width = 60;
			height = 60;
			break;
		case RUN_DUST:
			effectiveAnimation = new Texture("res/SpriteSheets/player/run_dust.png", 0, 0, 18, 18).loadAnimation(6);
			width = 32;
			height = 32;
			break;
		case EXPLOSION_RED:
			effectiveAnimation = new Texture("res/SpriteSheets/fx/explosion_red.png", 0, 0, 26, 22)
					.loadAnimation(effectNum);
			width = 78;
			height = 66;
			break;
		case EXPLOSION_SMOKE:
			effectiveAnimation = new Texture("res/SpriteSheets/fx/explosion_smoke.png", 0, 0, 72, 72)
					.loadAnimation(effectNum);

			width = 72;
			height = 72;
			break;
		default:
			effectiveAnimation = new Texture("res/SpriteSheets/fx/bullet_impact.png", 0, 0, 20, 20)
					.loadAnimation(effectNum);
			width = 60;
			height = 60;
			break;
		}
		effectiveAnimation.setDuration(effectNum * 3);

	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public int getDirection() {
		return direction;
	}

	public long getStartTime() {
		return startTime;
	}

	@Override
	public Animation getAnimation() {
		return effectiveAnimation;
	}
}
