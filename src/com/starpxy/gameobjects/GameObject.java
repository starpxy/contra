package com.starpxy.gameobjects;

import com.starpxy.physics2D.CircleRigidbody;

import com.starpxy.physics2D.Rigidbody;
import com.starpxy.physics2D.SimpleRectRigidbody;
import com.starpxy.util.Animation;
import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Vector3f;
/**
 * Edited by Xingyu Pan
 * @author starp
 *
 */
/*
 * Created by Abraham Campbell on 15/01/2020.
 *   Copyright (c) 2020  Abraham Campbell

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
   
   (MIT LICENSE ) e.g do what you want with this :-) 
 */
public class GameObject {

	private Point3f centre = new Point3f(0, 0, 0); // Centre of object, using 3D as objects may be scaled
	protected int width = 10;
	protected int height = 10;
	protected boolean hasTextured = false;
	protected Texture texture;
	protected String texturePath;
	protected String blanktexture = "res/blankSprite.png";

	// Create a rigidbody for the gameobject.
	private Rigidbody rigidbody = null;

	public GameObject() {

	}

	/**
	 * This is a method to return the animation of the game object. Subclasses need
	 * to rewrite it.
	 * 
	 * @return
	 */
	public Animation getAnimation() {
		return null;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public String getTexturePath() {
		return texturePath;
	}

	/**
	 * Create a simple rectangle rigidbody. Users can change other settings such as
	 * the gravity or kinematic status later.
	 * 
	 * @param width      is the height of the rigidbody
	 * @param height     is the height of the rigidbody
	 * @param mass       is the mass of the rigidbody
	 * @param initialPos is the initial position of the rigidbody
	 * @param initialRot is the initial rotation of the rigidbody
	 */
	public void addRectRigidbody(int width, int height, float mass, Point3f initialPos, Vector3f initialRot) {
		rigidbody = new SimpleRectRigidbody(width, height, mass, initialPos, initialRot);
	}

	/**
	 * Create a circle rigidbody. Users can change other settings such as the
	 * gravity status later.
	 * 
	 * @param redius     is the redius of the circle rigidbody
	 * @param mass       is the mass of the rigidbody
	 * @param initPos    is the initial position of the rigidbody
	 * @param initRotate is the initial rotation of the rigidbody
	 */
	public void addCircleRigidbody(int redius, float mass, Point3f initPos, Vector3f initRotate) {
		rigidbody = new CircleRigidbody(redius, mass, initPos, initRotate);
	}

	/**
	 * get the rigidbody
	 * 
	 * @return the rigidbody reference
	 */
	public Rigidbody getRigidbody() {
		return rigidbody;
	}

	public GameObject(String texturePath, int width, int height, Point3f centre) {
		hasTextured = true;
		this.texturePath = texturePath;
		this.width = width;
		this.height = height;
		this.centre = centre;
	}

	public GameObject(int width, int height, Point3f centre) {
		this.width = width;
		this.height = height;
		this.centre = centre;
	}

	public void syncPos() {
		if (rigidbody != null) {
			centre = rigidbody.position();
		}
	}

	public Point3f getCentre() {
		return centre;
	}

	public void setCentre(Point3f centre) {
		this.centre = centre;

		// make sure to put boundaries on the gameObject

	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Texture getTexture() {
		return texture;
	}
}

/*
 * Game Object
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * ::::::::::::::::::::::::::::::::::::::::::c::::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::clc:::::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::::::lol:;:::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * ::::::::::::::::::::::::::::::::::::::;;cool:;:::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::::::::::::codk0Oxolc::::::::::::::::::::::::::::::
 * :::::::::::::::::::::::
 * ::::::::::::::::::::::::::::::::coxk0XNWMMWWWNK0kxdolc:::::::::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::::::::loxO0XWMMMMMMMWWWMMMMMMWWNK0Oxdlc:::::::::::::::::
 * :::::::::::::::::::::::
 * :::::::::::::::::::::cldkOKNWMMMMMMMMMMMMMWWMMMMMMMMMMMMMWNX0Okdolc::::::::::
 * :::::::::::::::::::::::
 * ::::::::::::::::codk0KNWMMMMMMMMMMMMMMMMMMWMMMMMMMMMMMMMMMMMMMMWWXKOkxo::::::
 * :::::::::::::::::::::::
 * ::::::::::::;cdOXNWMMMMMMMMMMMMMMMMMMMMMMWWWMMMMMMMMMMMMMMMMMMMMMMMNKOdc:::::
 * :::::::::::::::::::::::
 * :::::::::::::cxKXXNWWMMMMMMMMMMMMMMMMMMMMWWWMMMMMMMMMMMMMMMMMMMWX0kdolc::::::
 * :::::::::::::::::::::::
 * ::::::::::::::d0000KKXNNWMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMWNKOxolllllc::::::
 * :::::::::::::::::::::::
 * ::::::::::::::oO00000000KXXNWWMMMMMMMMMMMMWMMMMMMMMMMMMMWX0kdollllllllc::::::
 * :::::::::::::::::::::::
 * ::::::::::::::lk00000O000000KKXNWWMMMMMMMWWWMMMMMMMMWNKOxollllllllllll:::::::
 * :::::::::::::::::::::::
 * ::::::::::::::cx0000000000O000000KXXNWMMMWWWMMMMWXK0kdlllllllllllllllc:::::::
 * :::::::::::::::::::::::
 * :::::::::::::::dO00000000000000000000KKXNNNWWNKOxolllllllllllllllllllc:::::::
 * :::::::::::::::::::::::
 * :::::::::::::::lO000000000000000OOOO0000000Kkdlllllllllllllllllllllllc:::::::
 * :::::::::::::::::::::::
 * :::::::::::::::ck00000000000000000OOOOOOOOkxollllllllllllllllllllllll::::::::
 * :::::::::::::::::::::::
 * :::::::::::::;;cx00000000000000000000OOOOOOxocllllllllllllllllllllllc:;;;;;;;
 * ;;;::::::::::::::::::::
 * ;;;;;;;;;;;;;;;:oO00000000000000000OOOO0000kdllllcclllllllllllllllllc:;;;;;;;
 * ;;;;;;;;;;;::::::::::::
 * ;;;;;;;;;;;;;;;:lO00000000000000OOO00000000Oolllllllllllllllllllllllc:;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;ck0000000000OOO000000000000kolllllllllllllllllllllll:;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;:dOO0000OOO0000000000000000kolllllllllllllllllllllllc:;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;:lxkOOOO0000000000000000000kolllllllllllllllllllllooool::;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;::;;::cokOkkO00000000000000000000kolllllllllllllllllllllccccllcc:::
 * :;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;:;;:;::ccllodxkO00000000000000000000kolllllllllllllllllllcc:;;;;:::::;
 * ;:;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;::::::::::;;:ldkO0000000000000000000kolllllllllllllllllc::;;::;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;::;;:::;;;;;:;::ldkO0000000000000000kollllllllllllllcc::;;;;:;;:;;;;;:
 * ;;;:;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;:;;;;:cldkO0000000000000kollllllllllllc:::;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;::ldkO0000000000kolllllllllcc::;;::;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;::;:;;::ldkO0000000kolllllllc::::;;;;:;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;:;::ldkO0000kolllcc:::;;;;;;;;::;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;:;;;;;:ldkO0kolcc::;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;:;;;::lodl:::::;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;:;::;:::::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;:;;::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;::;;;;;;;;;;;:::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 * ;;;;;;;;;;;;;;;;;;;;;;;
 */