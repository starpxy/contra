package com.starpxy.gameobjects;

import com.starpxy.util.Animation;
import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Vector3f;

/**
 * This is a class of bullet game object to simplify the initialization of a
 * bullet game object
 * 
 * @author xingyupan
 *
 */
public class BulletGameObject extends GameObject {
	private static float effectiveDistance = 400f;
	public static final int INIT_SIZE = 20;
	public static final float INIT_EFFECTIVE_DISTANCE = 400f;
	private static int size = 20;
	private float velocity = 10;
	private Animation durationAnimation;
	private Animation vanishAnimation;
	private Point3f initPos;

	public BulletGameObject(Point3f center, Vector3f shotDirection) {
		super("res/SpriteSheets/player/weapon_bullet.png", BulletGameObject.size, BulletGameObject.size, center);
		addRectRigidbody(BulletGameObject.size, BulletGameObject.size, 1, center, new Vector3f(0, 0, 0));
		initPos = new Point3f(center.getX(), center.getY(), center.getZ());
		getRigidbody().setVelocity(shotDirection.Normal().byScalar(velocity));
		durationAnimation = new Texture("res/SpriteSheets/player/weapon_bullet.png", 0, 0, 8, 8).loadAnimation(1);
		durationAnimation.setDuration(35);
		vanishAnimation = new Texture("res/SpriteSheets/player/weapon_bullet.png", 24, 0, 8, 8).loadAnimation(7);
		vanishAnimation.setDuration(15);
	}


	public boolean shoot() {
		Point3f position = getRigidbody().position();
		position.ApplyVector(getRigidbody().getVelocity());
		if (initPos.distanceOf(position) >= effectiveDistance) {
			return false;
		}
		getRigidbody().setPosition(position);
		syncPos();
		return true;
	}

	@Override
	public Animation getAnimation() {
		Point3f position = getRigidbody().position();
		if (initPos.distanceOf(position) < (0.9f * effectiveDistance)) {
			return durationAnimation;
		}
		return vanishAnimation;
	}

	public static int getSize() {
		return size;
	}

	public static void setSize(int size) {
		BulletGameObject.size = size;
	}

	public static float getEffectiveDistance() {
		return effectiveDistance;
	}

	public static void setEffectiveDistance(float effectiveDistance) {
		BulletGameObject.effectiveDistance = effectiveDistance;
	}
}
