package com.starpxy.gameobjects;

import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Vector3f;

public class ShelterGameObject extends GameObject {
	public ShelterGameObject(Point3f center) {
		super("res/SpriteSheets/props/shelter.png", 150, 120, center);
		this.texture = new Texture("res/SpriteSheets/props/shelter.png", 0, 0, 64, 42);
		this.addRectRigidbody(150, 30, 1, new Point3f(center.getX(),center.getY()-45,0), new Vector3f(0, 0, 0));
		this.getRigidbody().setGravityStatus(false);
		this.getRigidbody().setKinematicStatus(false);
	}
}
