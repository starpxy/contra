package com.starpxy.gameobjects;

import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Vector3f;

public class BasicGroundUnitGameObject extends GameObject {

	public BasicGroundUnitGameObject(Point3f center, int type) {
		setCentre(center);
		if (type == 0) {
			width = 300;
			height = 150;
			this.texture = new Texture("res/SpriteSheets/tileset.png", 8, 0, 96, 40);
			this.addRectRigidbody(280, 140, 1, center, new Vector3f(0, 0, 0));
			this.getRigidbody().setGravityStatus(false);
			this.getRigidbody().setKinematicStatus(false);
		} else {
			width = 240;
			height = 200;
			this.texture = new Texture("res/SpriteSheets/background/tile_cave_blob_1.png", 0, 0, 48, 40);
			this.addRectRigidbody(220, 180, 1, center, new Vector3f(0, 0, 0));
			this.getRigidbody().setGravityStatus(false);
			this.getRigidbody().setKinematicStatus(false);
		}
	}
	
}
