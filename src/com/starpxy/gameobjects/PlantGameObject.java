package com.starpxy.gameobjects;

import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Vector3f;

public class PlantGameObject extends GameObject {
	public PlantGameObject(Point3f center, int type) {
		setCentre(center);
		String path = "";
		switch (type) {
		case 1:
			path = "res/SpriteSheets/background/jungle_tree1.png";
			width = 200;
			height = 236;
			this.texture = new Texture(path, 0, 0, 50, 59);
			break;
		case 2:
			path = "res/SpriteSheets/background/jungle_tree2.png";
			width = 160;
			height = 236;
			this.texture = new Texture(path, 0, 0, 40, 59);
		default:
			break;
		}
		texturePath = path;
		if (type == 1) {
			this.addRectRigidbody(150, 80, 1, new Point3f(center.getX(), center.getY() - 65, 0), new Vector3f(0, 0, 0));
		} else {
			this.addRectRigidbody(100, 80, 1, new Point3f(center.getX(), center.getY() - 65, 0), new Vector3f(0, 0, 0));
		}
		this.getRigidbody().setGravityStatus(false);
		this.getRigidbody().setKinematicStatus(false);
	}
}
