package com.starpxy.gameobjects;

import com.starpxy.agents.EnemyAgent;
import com.starpxy.agents.base.Agent;
import com.starpxy.agents.base.AgentManager;
import com.starpxy.util.Animation;
import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Util;
import com.starpxy.util.Vector3f;

public class EnemyGameObject extends GameObject {
	private Animation runAnimation;
	private Animation idleAnimation;
	private Animation shotAnimation;
	private Animation dieAnimation;
	private float currentJumpSpeed = 0;
	private float jumpSpeed = 15;
	private long dieTime;
	private boolean isDead = false;
	private boolean isShooting = false;
	private boolean isRunning = false;
	private boolean isGround = false;
	private boolean isJumping = false;
	private boolean isMovable;
	private int horizontalDirection = Util.FORWARD;
	private long shootInterval = 100;
	private long lastShootTime;
	private float speedOfMovement = 2f;
	private Agent agent;

	public EnemyGameObject(Point3f center, boolean isMovable) {
//		setCentre(center);
		super(78, 66, center);
		this.isMovable = isMovable;
		runAnimation = new Texture("res/SpriteSheets/enemy/grunt_run.png", 0, 0, 26, 22).loadAnimation(10);
		runAnimation.setDuration(100);
		idleAnimation = new Texture("res/SpriteSheets/enemy/grunt_idle.png", 0, 0, 26, 22).loadAnimation(2);
		idleAnimation.setDuration(40);
		shotAnimation = new Texture("res/SpriteSheets/enemy/grunt_shoot.png", 0, 0, 26, 22).loadAnimation(2);
		shotAnimation.setDuration(40);
		dieAnimation = new Texture("res/SpriteSheets/enemy/grunt_death.png", 0, 0, 26, 22).loadAnimation(12);
		dieAnimation.setDuration(100);
		addRectRigidbody(50, 60, 1, center, new Vector3f(0, 0, 0));
		agent = new EnemyAgent();
		AgentManager manager = AgentManager.getInstance();
		manager.registerAgent(agent);
	}

	@Override
	public Animation getAnimation() {
		if (isDead) {
			return dieAnimation;
		} else {
			if (isShooting) {
				return shotAnimation;
			} else if (isRunning) {
				return runAnimation;
			} else {
				return idleAnimation;
			}
		}
	}

	/*
	 * The enemies are not allowed to shot while running
	 */

	public void initFrame() {
		getRigidbody().unfreezeX();
		getRigidbody().unfreezeY();
		isRunning = false;
		isGround = false;
		isShooting = false;
	}

	public void moveForward() {
		horizontalDirection = Util.FORWARD;
		isRunning = true;
		isShooting = false;
		if (getRigidbody().isXFrozen()) {
			return;
		}
		getRigidbody().setVelocity(new Vector3f(getSpeedOfMovement(), 0, 0));
		Point3f temp = this.getRigidbody().position();
		temp.ApplyVector(new Vector3f(this.getSpeedOfMovement(), 0, 0));
		this.getRigidbody().setPosition(temp);
		this.syncPos();
	}

	public void moveBackward() {
		horizontalDirection = Util.BACK;
		isRunning = true;
		isShooting = false;
		Point3f temp = this.getRigidbody().position();
		getRigidbody().setVelocity(new Vector3f(getSpeedOfMovement(), 0, 0));
		temp.ApplyVector(new Vector3f(-this.getSpeedOfMovement(), 0, 0));
		this.getRigidbody().setPosition(temp);
		this.syncPos();
	}

	public void jump() {
		if (!isJumping) {
			isJumping = true;
			isGround = false;
			currentJumpSpeed = jumpSpeed;
			Point3f temp = getRigidbody().position();
			temp.ApplyVector(new Vector3f(0, currentJumpSpeed, 0));
			getRigidbody().setPosition(temp);
			syncPos();
		}
	}

	public boolean shoot() {
		if (!isRunning) {
			if (Util.getCurrentTime() - lastShootTime > shootInterval) {
				isShooting = true;
				lastShootTime = Util.getCurrentTime();
				return true;
			}
		}
		return false;
	}

	public void setHorizontalDirection(int horizontalDirection) {
		this.horizontalDirection = horizontalDirection;
	}

	public int getHorizontalDirection() {
		return horizontalDirection;
	}

	public long getDieTime() {
		return dieTime;
	}

	public boolean isShooting() {
		return isShooting;
	}

	public void setDead() {
		this.isDead = true;
		dieTime = Util.getCurrentTime();
	}

	public boolean isDead() {
		return isDead;
	}

	public boolean isMovable() {
		return isMovable;
	}

	public void setSpeedOfMovement(float speedOfMovement) {
		this.speedOfMovement = speedOfMovement;
	}

	public float getSpeedOfMovement() {
		return speedOfMovement;
	}

	public boolean isGround() {
		return isGround;
	}

	public void setGround(boolean isGround) {
		this.isGround = isGround;
	}

	public boolean isJumping() {
		return isJumping;
	}

	public void setJumping(boolean isJumping) {
		this.isJumping = isJumping;
	}

	public float getCurrentJumpSpeed() {
		return currentJumpSpeed;
	}

	public void setCurrentJumpSpeed(float currentJumpSpeed) {
		this.currentJumpSpeed = currentJumpSpeed;
	}

	public Agent getAgent() {
		return agent;
	}
}
