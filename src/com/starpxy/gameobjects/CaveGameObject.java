package com.starpxy.gameobjects;

import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Vector3f;

public class CaveGameObject extends GameObject {
	public CaveGameObject(Point3f center) {
		super("res/SpriteSheets/background/tile_cave_blob_1.png", 240, 200, center);
		this.texture = new Texture(this.getTexturePath(), 0, 0, 48, 40);
		this.addRectRigidbody(220, 180, 1, center, new Vector3f(0, 0, 0));
		this.getRigidbody().setGravityStatus(false);
		this.getRigidbody().setKinematicStatus(false);
	}
}
