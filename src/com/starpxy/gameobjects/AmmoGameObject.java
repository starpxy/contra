package com.starpxy.gameobjects;

import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Vector3f;

/**
 * (v1.0) The ammo has different types.
 * 
 * These are:
 * 
 * 1) Range UP: 1;
 * 
 * 2) Size UP: 2;
 * 
 * 3) Rate UP: 3;
 * 
 * @author xingyupan
 *
 */
public class AmmoGameObject extends GameObject {
	private int ammoType;
	private float initialSpeed = 10;
	private Point3f typeStartPos = new Point3f(24, 12, 0);
	private Point3f typeEndPos = new Point3f(48, 39, 0);
	private float currentSpeed;
	private Texture typeTexture;
	private boolean isGround = false;

	public AmmoGameObject(Point3f center, int type) {
		super("res/SpriteSheets/props/ammo.png", 48, 39, center);
		this.ammoType = type;
		texture = new Texture("res/SpriteSheets/props/ammo.png", 0, 0, 16, 13);
		switch (type) {
		case 1:
			typeTexture = new Texture("res/SpriteSheets/ui/ui_weapon_tribow_blue.png", 0, 0, 13, 13);
			break;
		case 2:
			typeTexture = new Texture("res/SpriteSheets/ui/ui_weapon_target_red.png", 0, 0, 13, 13);
			break;
		case 3:
			typeTexture = new Texture("res/SpriteSheets/ui/ui_weapon_repeater_blue.png", 0, 0, 13, 13);
			break;
		default:
			typeTexture = new Texture("res/SpriteSheets/ui/ui_weapon_tribow_blue.png", 0, 0, 13, 13);
			break;
		}
		currentSpeed = initialSpeed;
		addRectRigidbody(48, 38, 1, center, new Vector3f());
	}

	public void initFrame() {
		isGround = false;
	}

	public Texture getTypeTexture() {
		return typeTexture;
	}

	public float getInitialSpeed() {
		return initialSpeed;
	}

	public float getCurrentSpeed() {
		return currentSpeed;
	}

	public void setCurrentSpeed(float currentSpeed) {
		this.currentSpeed = currentSpeed;
	}

	public int getAmmoType() {
		return ammoType;
	}

	public Point3f getTypeStartPos() {
		return typeStartPos;
	}

	public Point3f getTypeEndPos() {
		return typeEndPos;
	}

	public boolean isGround() {
		return isGround;
	}

	public void setGround(boolean isGround) {
		this.isGround = isGround;
	}
}
