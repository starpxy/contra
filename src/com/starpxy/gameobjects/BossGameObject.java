package com.starpxy.gameobjects;

import java.awt.Graphics;

import com.starpxy.music.MusicPlayer;
import com.starpxy.util.Animation;
import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Util;
import com.starpxy.util.Vector3f;

/**
 * Only the boss head has the rigidbody to be collided
 */
public class BossGameObject extends GameObject {

	/*
	 * The boss has following status:
	 * 
	 * 1) 0: player come in and play start animation;
	 * 
	 * 2) 1: shoot laser (will add next version)
	 * 
	 * 3) 2: shoot from mouth
	 * 
	 * 4) 3: hide and throw small enemies
	 * 
	 * 5) 4: die
	 * 
	 * 6) 5: player died and the Boss won
	 * 
	 * 7) 6: idle
	 */

	// pre-set a execute order and make sure this can definately kill the player or
	// be killed by the player
	private int[] executeOrder = { 0, 2, 6, 3, 6, 2, 6, 3, 6, 2, 6, 3, 6, 2, 6, 3, 6, 3, 6, 3, 6 };
	private int status;
	private Animation[] bossIntroBGAnimations;
	private Animation headIdleAnimation;
	private Animation bodyIdleAnimation;
	private Animation headShootAnimation;
	private Point3f headPos;
	private Point3f bodyPos;
	private Point3f currentHeadPos;
	private Point3f currentBodyPos;

	private int bodyWidth;
	private int bodyHeight;
	private int headWidth;
	private int headHeight;
	private long startTimestamp;

	private int health;
	private long shottInterval;
	private long idleInterval;
	private long executeTime = 600;
	private long introTime = 600;
	private boolean isMoving;
	private boolean isIntroduced = false;
	private boolean isDie = false;
	private long lastGeneratedTimestamp;

	private Point3f[] caveGround;

	public BossGameObject(Point3f center, int health, long idleInterval) {
		// initialize the domain variables.
		startTimestamp = Util.getCurrentTime();
		status = 0;
		isMoving = false;
		this.health = health;
		this.shottInterval = 60;
		this.idleInterval = idleInterval;
		bodyHeight = 210;
		bodyWidth = 540;
		headHeight = 220;
		headWidth = 150;
		setCentre(center);

		// init cave ground
		caveGround = new Point3f[23];
		for (int i = 0; i < 6; i++) {
			caveGround[i] = new Point3f(100 + 200 * i, 30, 0);
		}
		for (int i = 0; i < 7; i++) {
			caveGround[i + 6] = new Point3f(100 + 200 * i, 720, 0);
		}
		for (int i = 0; i < 5; i++) {
			caveGround[i + 13] = new Point3f(65, 60 + 120 * i, 0);
		}
		for (int i = 0; i < 5; i++) {
			caveGround[i + 18] = new Point3f(1215, 60 + 120 * i, 0);
		}

		// this is the final position of the head and body positions
		headPos = new Point3f(center.getX(), center.getY() - 20, 0);
		bodyPos = new Point3f(center.getX(), center.getY() + bodyHeight / 2, 0);

		// init current head position.
		currentHeadPos = new Point3f(center.getX(), center.getY() + 480, 0);
		currentBodyPos = new Point3f(center.getX(), center.getY() + 500 + bodyHeight / 2, 0);

		// this rigidbody is for collision detection.
		addRectRigidbody(headWidth - 20, headHeight - 20, 1, currentHeadPos, new Vector3f());

		// initialize the animations
		bossIntroBGAnimations = new Animation[3];
		bossIntroBGAnimations[0] = new Texture("res/SpriteSheets/boss/BossScreen_Anim_intro.png", 0, 0, 320, 180)
				.loadAnimation(10);
		bossIntroBGAnimations[0].setDuration(introTime / 3);
		bossIntroBGAnimations[1] = new Texture("res/SpriteSheets/boss/BossScreen_Anim_intro.png", 0, 180, 320, 180)
				.loadAnimation(10);
		bossIntroBGAnimations[1].setDuration(introTime / 3);
		bossIntroBGAnimations[2] = new Texture("res/SpriteSheets/boss/BossScreen_Anim_intro.png", 0, 360, 320, 180)
				.loadAnimation(10);
		bossIntroBGAnimations[2].setDuration(introTime / 3);
		headIdleAnimation = new Texture("res/SpriteSheets/boss/BossHeadIdle.png", 0, 0, 47, 67).loadAnimation(12);
		headIdleAnimation.setDuration(120);
		headShootAnimation = new Texture("res/SpriteSheets/boss/BossScreen_Anim_openmouth_fire.png", 0, 0, 47, 67)
				.loadAnimation(12);
		headShootAnimation.setDuration(120);
		bodyIdleAnimation = new Texture("res/SpriteSheets/boss/BossBodyIdle.png", 0, 0, 180, 71).loadAnimation(12);
		bodyIdleAnimation.setDuration(120);

	}

	/**
	 * update the animation status according to the current time.
	 * 
	 * @return true if the game is not end; false if the game is end.
	 */
	public boolean update(boolean isMute) {
		long passedTime = Util.getCurrentTime() - startTimestamp;
		if (passedTime < introTime) {
			status = executeOrder[0];
			if (passedTime > introTime / 2) {
				showBoss(isMute);
			}
		} else if (status == 4) {
			// the boss died.
			if (!isDie) {
				isDie = true;
			}
			hideBoss();
			return false;
		} else if (status == 5) {
			// player died.
			showBoss(isMute);
			return false;
		} else {
			long temp = passedTime - introTime;
			int index = 0;
			while (temp > 0) {
				if (index % 2 == 0) {
					temp -= executeTime;
				} else {
					temp -= idleInterval;
				}
				index++;
			}
			status = executeOrder[index];
			if (status == 3) {
				hideBoss();
			} else {
				showBoss(isMute);
			}
		}
		return true;
	}

	public void hit(boolean isMute) {
		health -= BulletGameObject.getSize();
		if (!isMute) {
			MusicPlayer.getInstance().playBossInjured();
		}
		if (health <= 0) {
			// the boss die
			status = 4;
		}
	}

	public void renderCache(Graphics g) {
		Texture texture = new Texture("res/SpriteSheets/boss/BossScreen_rockcache2.png", 0, 0, 320, 180);
		g.drawImage(texture.getTextureImage(), 0, 0, 1280, 720, 0, 0, 320, 180, null);
	}

	private void showBoss(boolean isMute) {
		if (currentBodyPos.getY() > bodyPos.getY()) {
			isMoving = true;
			currentBodyPos.ApplyVector(new Vector3f(0, 2, 0));
			Point3f tempPoint3f = getRigidbody().position();
			tempPoint3f.ApplyVector(new Vector3f(0, 2, 0));
			getRigidbody().setPosition(tempPoint3f);
			syncHeadPos();
			if (currentBodyPos.getY() < bodyPos.getY() + 300 && !isIntroduced) {
				isIntroduced = true;
				if (!isMute) {
					MusicPlayer.getInstance().playBossIntro();
				}
			}
		} else {
			isMoving = false;
		}
	}

	public void renderBossIdle(Graphics g) {
		int currentFrame = (int) (Util.getCurrentTime() % 120 * 12 / 120);
		Texture bodyTexture = bodyIdleAnimation.getAnimationTextures()[currentFrame];
		Texture headTexture = headIdleAnimation.getAnimationTextures()[currentFrame];
		int headX = (int) (currentHeadPos.getX() - headWidth / 2);
		int headY = (int) (currentHeadPos.getY() - headHeight / 2);
		int bodyX = (int) (currentBodyPos.getX() - bodyWidth / 2);
		int bodyY = (int) (currentBodyPos.getY() - bodyHeight / 2);
		g.drawImage(bodyTexture.getTextureImage(), bodyX, bodyY, bodyX + bodyWidth, bodyY + bodyHeight,
				bodyTexture.getX(), bodyTexture.getY(), bodyTexture.getX() + bodyTexture.getWidth(),
				bodyTexture.getY() + bodyTexture.getHeight(), null);
		g.drawImage(headTexture.getTextureImage(), headX, headY, headX + headWidth, headY + headHeight,
				headTexture.getX(), headTexture.getY(), headTexture.getX() + headTexture.getWidth(),
				headTexture.getY() + headTexture.getHeight(), null);
	}

	private void hideBoss() {
		if (currentBodyPos.getY() < bodyPos.getY() + 500) {
			isMoving = true;
			currentBodyPos.ApplyVector(new Vector3f(0, -2, 0));
			Point3f tempPoint3f = getRigidbody().position();
			tempPoint3f.ApplyVector(new Vector3f(0, -2, 0));
			getRigidbody().setPosition(tempPoint3f);
			syncHeadPos();
		} else {
			isMoving = false;
		}
	}

	private void syncHeadPos() {
		currentHeadPos = getRigidbody().position();
	}

	private void shake() {

	}

	public int getStatus() {
		return status;
	}

	public Point3f getCurrentBodyPos() {
		return currentBodyPos;
	}

	public Point3f getCurrentHeadPos() {
		return currentHeadPos;
	}

	public int getHeadHeight() {
		return headHeight;
	}

	public int getBodyHeight() {
		return bodyHeight;
	}

	public int getHeadWidth() {
		return headWidth;
	}

	public int getBodyWidth() {
		return bodyWidth;
	}

	public long getStartTimestamp() {
		return startTimestamp;
	}

	public Animation getBodyIdleAnimation() {
		return bodyIdleAnimation;
	}

	public Animation getBossIntroBGAnimation() {
		if (Util.getCurrentTime() - startTimestamp < introTime / 3) {
			return bossIntroBGAnimations[0];
		} else if (Util.getCurrentTime() - startTimestamp < 2 * introTime / 3) {
			return bossIntroBGAnimations[1];
		} else {
			return bossIntroBGAnimations[2];
		}
	}

	public Point3f[] getCaveGround() {
		return caveGround;
	}

	public Animation getHeadIdleAnimation() {
		return headIdleAnimation;
	}

	public boolean isMoving() {
		return isMoving;
	}

	public long getLastGeneratedTimestamp() {
		return lastGeneratedTimestamp;
	}

	public void setLastGeneratedTimestamp(long lastGeneratedTimestamp) {
		this.lastGeneratedTimestamp = lastGeneratedTimestamp;
	}

	public long getIntroTime() {
		return introTime;
	}

	public long getExecuteTime() {
		return executeTime;
	}

	public int getHealth() {
		return health;
	}

	public long getShottInterval() {
		return shottInterval;
	}

	public Point3f getHeadPos() {
		return headPos;
	}

}
