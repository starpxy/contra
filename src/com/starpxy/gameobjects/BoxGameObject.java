package com.starpxy.gameobjects;

import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Vector3f;

/**
 * Create a box gameobject that can explode when user shoot it a couple of
 * times.
 * 
 * @author xingyupan
 *
 */
public class BoxGameObject extends GameObject {
	private int maxHitTime = 2;
	private int hitTime = 0;
	private int type;

	public BoxGameObject(Point3f center, int type) {
		setCentre(center);
		this.type = type;
		if (type == 1) {
			texturePath = "res/SpriteSheets/props/crate.png";
			width = 48;
			height = 48;
			texture = new Texture(texturePath, 0, 0, 16, 16);
			addRectRigidbody(40, 40, 1, center, new Vector3f(0, 0, 0));
		} else if (type == 2) {
			texturePath = "res/SpriteSheets/props/barrel.png";
			width = 39;
			height = 48;
			texture = new Texture(texturePath, 0, 0, 13, 16);
			addRectRigidbody(35, 40, 1, center, new Vector3f(0, 0, 0));
		} else {
			texturePath = "res/SpriteSheets/props/bigcrate.png";
			width = 96;
			height = 48;
			texture = new Texture(texturePath, 0, 0, 32, 16);
			addRectRigidbody(88, 40, 1, center, new Vector3f(0, 0, 0));
		}

		this.getRigidbody().setGravityStatus(false);
		this.getRigidbody().setKinematicStatus(false);
	}

	public void hit() {
		hitTime++;
	}

	public boolean isExploded() {
		if (hitTime >= maxHitTime) {
			return true;
		} else {
			return false;
		}
	}

	public int getType() {
		return type;
	}
}
