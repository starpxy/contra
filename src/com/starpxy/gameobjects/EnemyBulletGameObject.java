package com.starpxy.gameobjects;

import com.starpxy.util.Animation;
import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Vector3f;

public class EnemyBulletGameObject extends GameObject {
	private static float effectiveDistance = 500f;
	private static int size = 20;
	private float velocity = 3;
	private Animation durationAnimation;
	private Animation vanishAnimation;
	private Point3f initPos;

	public EnemyBulletGameObject(Point3f center, Vector3f shotDirection) {
		super("res/SpriteSheets/enemy/enemy_weapon_bullet.png", EnemyBulletGameObject.size, EnemyBulletGameObject.size,
				center);
		addRectRigidbody(EnemyBulletGameObject.size, EnemyBulletGameObject.size, 1, center, new Vector3f(0, 0, 0));
		initPos = new Point3f(center.getX(), center.getY(), center.getZ());
		getRigidbody().setVelocity(shotDirection.Normal().byScalar(velocity));
		durationAnimation = new Texture("res/SpriteSheets/enemy/enemy_weapon_bullet.png", 0, 0, 8, 8).loadAnimation(1);
		durationAnimation.setDuration(35);
		vanishAnimation = new Texture("res/SpriteSheets/enemy/enemy_weapon_bullet.png", 24, 0, 8, 8).loadAnimation(7);
		vanishAnimation.setDuration(15);
	}

	public EnemyBulletGameObject(Point3f center, Vector3f shotDirection, boolean boss) {
		super("res/SpriteSheets/boss/Boss_Fire.png", EnemyBulletGameObject.size * 3, EnemyBulletGameObject.size * 3,
				center);
		addRectRigidbody(EnemyBulletGameObject.size * 3, EnemyBulletGameObject.size * 3, 1, center,
				new Vector3f(0, 0, 0));
		initPos = new Point3f(center.getX(), center.getY(), center.getZ());
		getRigidbody().setVelocity(shotDirection.Normal().byScalar(velocity));
		durationAnimation = new Texture("res/SpriteSheets/boss/Boss_Fire.png", 0, 0, 32, 32).loadAnimation(3);
		durationAnimation.setDuration(50);
		vanishAnimation = new Texture("res/SpriteSheets/boss/Boss_Fire_Expl.png", 0, 0, 32, 32).loadAnimation(8);
		vanishAnimation.setDuration(20);
	}

	public boolean shoot() {
		Point3f position = getRigidbody().position();
		position.ApplyVector(getRigidbody().getVelocity());
		if (initPos.distanceOf(position) >= effectiveDistance) {
			return false;
		}
		getRigidbody().setPosition(position);
		syncPos();
		return true;
	}

	@Override
	public Animation getAnimation() {
		Point3f position = getRigidbody().position();
		if (initPos.distanceOf(position) < (0.9f * effectiveDistance)) {
			return durationAnimation;
		}
		return vanishAnimation;
	}

	public static int getSize() {
		return size;
	}

	public static void setSize(int size) {
		EnemyBulletGameObject.size = size;
	}

	public static float getEffectiveDistance() {
		return effectiveDistance;
	}

	public static void setEffectiveDistance(float effectiveDistance) {
		EnemyBulletGameObject.effectiveDistance = effectiveDistance;
	}
}
