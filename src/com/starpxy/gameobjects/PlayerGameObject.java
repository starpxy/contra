package com.starpxy.gameobjects;

import com.badlogic.gdx.graphics.g3d.Model;
import com.starpxy.music.MusicPlayer;
import com.starpxy.util.Animation;
import com.starpxy.util.Point3f;
import com.starpxy.util.Texture;
import com.starpxy.util.Util;
import com.starpxy.util.Vector3f;

/**
 * This is a class for player gameobject, the class extends Abey's origin
 * gameobject class, and added the movement speed, faceDirection and status as
 * the attributes.
 * 
 * @author xingyupan
 *
 */
public class PlayerGameObject extends GameObject {
	private int lifeNum = 5;
	private int speedOfMovement = 3;
	private float jumpSpeed = 15;
	private int horizontalDirection = Util.FORWARD;
	private int verticalDirection = Util.DEFAULT;
	private int status = Util.IDLE;
	private boolean isShooting = false;
	private boolean isJumping = false;
	private boolean isGround = false;
	private boolean isUnbreakable = false;
	private long unbreakableLastTime = 200;
	private long lastUnbreakableTimestamp;
	private long lastShotTimestamp;
	private long lastHurtTimestamp;
	private long dieTimestamp;
	private float currentJumpSpeed;
	private float maxFallingSpeed = -15;
	private boolean isAllLifeDied = false;
	private boolean doubleJump = false;

	private long shootInterval = 25;

	private Animation idleAnimation;
	private Animation idleShotAnimation;
	private Animation idleLookUpAnimation;
	private Animation idleLookdownAnimation;
	private Animation idleShotUpAnimation;
	private Animation idleShotDownAnimation;
	private Animation runAnimation;
	private Animation runShotAnimation;
	private Animation runShotUpAnimation;
	private Animation runShotDownAnimation;
	private Animation runLookUpAnimation;
	private Animation runLookDownAnimation;
	private Animation hitHurtAnimation;
	private Animation defeatAnimation;
	private Animation dieAnimation;
	private Animation soulOutAnimation;

	public PlayerGameObject(Point3f centre) {
		super(78, 66, centre);
		this.addRectRigidbody(50, 60, 1, centre, new Vector3f(0, 0, 0));
		this.getRigidbody().setKinematicStatus(true);
		this.getRigidbody().setGravityStatus(true);
		// initialize the animations
		idleAnimation = new Texture("res/SpriteSheets/player/john_idle.png", 0, 0, 26, 22).loadAnimation(5);
		idleAnimation.setDuration(50);
		idleShotAnimation = new Texture("res/SpriteSheets/player/john_stand_shooting.png", 0, 0, 26, 22)
				.loadAnimation(2);
		idleShotAnimation.setDuration(20);
		idleLookUpAnimation = new Texture("res/SpriteSheets/player/john_stand_looking_up.png", 0, 0, 26, 22)
				.loadAnimation(1);
		idleLookUpAnimation.setDuration(10);
		idleLookdownAnimation = new Texture("res/SpriteSheets/player/john_stand_looking_down_diag.png", 0, 0, 26, 22)
				.loadAnimation(1);
		idleLookdownAnimation.setDuration(10);
		idleShotDownAnimation = new Texture("res/SpriteSheets/player/john_stand_shooting_down_diag.png", 0, 0, 26, 22)
				.loadAnimation(2);
		idleShotDownAnimation.setDuration(20);
		idleShotUpAnimation = new Texture("res/SpriteSheets/player/john_stand_shooting_up.png", 0, 0, 26, 22)
				.loadAnimation(2);
		idleShotUpAnimation.setDuration(20);
		runAnimation = new Texture("res/SpriteSheets/player/john_run.png", 0, 0, 26, 22).loadAnimation(10);
		runAnimation.setDuration(50);
		runShotAnimation = new Texture("res/SpriteSheets/player/john_run_shoot.png", 0, 0, 26, 22).loadAnimation(10);
		runShotAnimation.setDuration(50);
		runShotUpAnimation = new Texture("res/SpriteSheets/player/john_run_shoot_up_diag.png", 0, 0, 26, 22)
				.loadAnimation(10);
		runShotUpAnimation.setDuration(50);
		runShotDownAnimation = new Texture("res/SpriteSheets/player/john_run_NG_diag_aimdowndiag.png", 0, 0, 26, 22)
				.loadAnimation(10);
		runShotDownAnimation.setDuration(50);
		runLookUpAnimation = new Texture("res/SpriteSheets/player/john_run_up_diag.png", 0, 0, 26, 22)
				.loadAnimation(10);
		runLookUpAnimation.setDuration(50);
		runLookDownAnimation = new Texture("res/SpriteSheets/player/john_run_NG_diag_aimdowndiag.png", 0, 0, 26, 22)
				.loadAnimation(10);
		runLookDownAnimation.setDuration(50);
		hitHurtAnimation = new Texture("res/SpriteSheets/player/john_hithurt.png", 0, 0, 26, 22).loadAnimation(2);
		hitHurtAnimation.setDuration(10);
		defeatAnimation = new Texture("res/SpriteSheets/player/john_defeated.png", 0, 0, 26, 22).loadAnimation(10);
		defeatAnimation.setDuration(100);
		dieAnimation = new Texture("res/SpriteSheets/player/john_dead.png", 0, 0, 26, 22).loadAnimation(2);
		dieAnimation.setDuration(50);
		soulOutAnimation = new Texture("res/SpriteSheets/player/john_dead_soul.png", 0, 0, 26, 22).loadAnimation(2);
		soulOutAnimation.setDuration(10);
	}

	/**
	 * The method will return the animation according to the player status
	 */
	@Override
	public Animation getAnimation() {
		if (status == Util.IDLE) {
			switch (verticalDirection) {
			case Util.DEFAULT:
				if (isShooting) {
					return idleShotAnimation;
				} else {
					return idleAnimation;
				}
			case Util.UP:
				if (isShooting) {
					return idleShotUpAnimation;
				} else {
					return idleLookUpAnimation;
				}
			case Util.DOWN:
				if (isShooting) {
					return idleShotDownAnimation;
				} else {
					return idleLookdownAnimation;
				}
			default:
				if (isShooting) {
					return idleShotAnimation;
				} else {
					return idleAnimation;
				}
			}
		} else if (status == Util.RUN) {
			switch (verticalDirection) {
			case Util.DEFAULT:
				if (isShooting) {
					return runShotAnimation;
				} else {
					return runAnimation;
				}
			case Util.UP:
				if (isShooting) {
					return runShotUpAnimation;
				} else {
					return runLookUpAnimation;
				}
			case Util.DOWN:
				if (isShooting) {
					return runShotDownAnimation;
				} else {
					return runLookDownAnimation;
				}
			default:
				if (isShooting) {
					return runShotAnimation;
				} else {
					return runAnimation;
				}
			}
		} else if (status == Util.HURT) {
			return hitHurtAnimation;
		} else if (status == Util.DEFEATED) {
			return defeatAnimation;
		} else if (status == Util.DEAD) {
			return dieAnimation;
		} else if (status == Util.SOUL) {
			return soulOutAnimation;
		}
		return null;
	}

	public void moveForward() {
		horizontalDirection = Util.FORWARD;
		status = Util.RUN;
		if (getRigidbody().isXFrozen()) {
			return;
		}
		getRigidbody().setVelocity(new Vector3f(getSpeedOfMovement(), 0, 0));
		Point3f temp = this.getRigidbody().position();
		temp.ApplyVector(new Vector3f(this.getSpeedOfMovement(), 0, 0));
		this.getRigidbody().setPosition(temp);
		this.syncPos();
	}

	public void moveBackward() {
		horizontalDirection = Util.BACK;
		status = Util.RUN;
		if (getRigidbody().isYFrozen()) {
			return;
		}
		Point3f temp = this.getRigidbody().position();
		getRigidbody().setVelocity(new Vector3f(getSpeedOfMovement(), 0, 0));
		temp.ApplyVector(new Vector3f(-this.getSpeedOfMovement(), 0, 0));
		this.getRigidbody().setPosition(temp);
		this.syncPos();
	}

	/**
	 * Change all the status back to default when user doesn't operate
	 */
	public void initFrame() {
		verticalDirection = Util.DEFAULT;
		isShooting = false;
		getRigidbody().unfreezeX();
		getRigidbody().unfreezeY();
		isGround = false;
		if (status == Util.DEFEATED) {
			if (Util.getCurrentTime() - dieTimestamp > 100) {
				status = Util.DEAD;
			}
		} else if (status == Util.DEAD) {
			if (Util.getCurrentTime() - dieTimestamp > 150) {
				status = Util.SOUL;
			}
		} else if (status == Util.SOUL) {
			Point3f temp = getRigidbody().position();
			temp.ApplyVector(new Vector3f(0, 2, 0));
			getRigidbody().setPosition(temp);
			syncPos();
		} else if (status == Util.HURT) {
			if ((Util.getCurrentTime() - lastHurtTimestamp) > 10) {
				status = Util.IDLE;
				getRigidbody().setKinematicStatus(true);
				isUnbreakable = true;
			}
		} else if (isUnbreakable) {
			if ((Util.getCurrentTime() - lastUnbreakableTimestamp) > unbreakableLastTime) {
				isUnbreakable = false;
			}
		} else if (!isJumping) {
			status = Util.IDLE;
			getRigidbody().setKinematicStatus(true);
		}
	}

	public void jump(boolean isMute) {
		if (!isJumping || (isJumping && !doubleJump)) {
			if (!isMute) {
				MusicPlayer.getInstance().playJumpSound();
			}
			if (isJumping) {
				doubleJump = true;
			}
			isJumping = true;
			isGround = false;
			currentJumpSpeed = jumpSpeed;
			Point3f temp = getRigidbody().position();
			temp.ApplyVector(new Vector3f(0, currentJumpSpeed, 0));
			getRigidbody().setPosition(temp);
			syncPos();
		}
	}

	public void hurt(boolean isMute) {
		if (lifeNum > 1) {
			if (!isMute) {
				MusicPlayer.getInstance().playHurtSound();
			}
			lastHurtTimestamp = Util.getCurrentTime();
			lastUnbreakableTimestamp = lastHurtTimestamp;
			getRigidbody().setKinematicStatus(false);
			status = Util.HURT;
		} else {
			if (!isMute) {
				MusicPlayer.getInstance().playPlayerDieSound();
			}
			status = Util.DEFEATED;
			dieTimestamp = Util.getCurrentTime();
			getRigidbody().setKinematicStatus(false);
			isUnbreakable = false;
		}
		lifeNum -= 1;
	}

	public void setJumping(boolean isJumping) {
		this.isJumping = isJumping;
	}

	public void shot() {
		isShooting = true;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getHorizontalDirection() {
		return horizontalDirection;
	}

	public int getVerticalDirection() {
		return verticalDirection;
	}

	public boolean isJumping() {
		return isJumping;
	}

	public void lookUp() {
		verticalDirection = Util.UP;
	}

	public void lookDown() {
		verticalDirection = Util.DOWN;
	}

	public void setOnGround() {
		isGround = true;
	}

	public boolean isGround() {
		return isGround;
	}

	public boolean isShooting() {
		return isShooting;
	}

	public float getJumpSpeed() {
		return jumpSpeed;
	}

	public void resetJumpSpeed(float speed) {
		this.jumpSpeed = speed;
	}

	public int getStatus() {
		return status;
	}

	public void setLastShotTimestamp(long lastShotTimestamp) {
		this.lastShotTimestamp = lastShotTimestamp;
	}

	public long getLastShotTimestamp() {
		return lastShotTimestamp;
	}

	public void setShootInterval(long shootInterval) {
		this.shootInterval = shootInterval;
	}

	public long getShootInterval() {
		return shootInterval;
	}

	public float getCurrentJumpSpeed() {
		return currentJumpSpeed;
	}

	public float getMaxFallingSpeed() {
		return maxFallingSpeed;
	}

	public void setCurrentJumpSpeed(float currentJumpSpeed) {
		if (currentJumpSpeed < maxFallingSpeed) {
			currentJumpSpeed = maxFallingSpeed;
		}
		this.currentJumpSpeed = currentJumpSpeed;
	}

	public int getLifeNum() {
		return lifeNum;
	}

	public long getLastHurtTimestamp() {
		return lastHurtTimestamp;
	}

	public long getDieTimestamp() {
		return dieTimestamp;
	}

	public void setSpeedOfMovement(int speedOfMovement) {
		this.speedOfMovement = speedOfMovement;
	}

	public int getSpeedOfMovement() {
		return speedOfMovement;
	}

	public boolean isUnbreakable() {
		return isUnbreakable;
	}

	public boolean isAllLifeDied() {
		return isAllLifeDied;
	}

	public boolean isDoubleJump() {
		return doubleJump;
	}

	public void setDoubleJump(boolean doubleJump) {
		this.doubleJump = doubleJump;
	}
}
