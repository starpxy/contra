package com.starpxy.map;

import com.starpxy.util.Point3f;

/**
 * Entity class for load a Map from JSON files
 * 
 * @author xingyupan
 *
 */
public class Map {
	private Point3f[] groundPos;
	private Point3f[] shelterPos;
	private Point3f[] plantPos;
	private Point3f[] boxPos;
	private Point3f[] movableEnemyPos;
	private Point3f[] frozeenEnemyPos;
	private int endX;
	private int newBackgroundX;

	private int bossHealth;
	private long waveInterval;

	public Map(Point3f[] groundPos, Point3f[] shelterPos, Point3f[] plantPos, Point3f[] boxPos,
			Point3f[] movableEnemyPos, Point3f[] frozeenEnemyPos, int endX, int newBackgroundX, int bossHealth,
			long waveInterval) {
		super();
		this.groundPos = groundPos;
		this.shelterPos = shelterPos;
		this.plantPos = plantPos;
		this.boxPos = boxPos;
		this.movableEnemyPos = movableEnemyPos;
		this.frozeenEnemyPos = frozeenEnemyPos;
		this.endX = endX;
		this.newBackgroundX = newBackgroundX;
		this.bossHealth = bossHealth;
		this.waveInterval = waveInterval;
	}

	public Point3f[] getBoxPos() {
		return boxPos;
	}

	public void setBoxPos(Point3f[] boxPos) {
		this.boxPos = boxPos;
	}

	public int getEndX() {
		return endX;
	}

	public void setEndX(int endX) {
		this.endX = endX;
	}

	public Point3f[] getPlantPos() {
		return plantPos;
	}

	public void setPlantPos(Point3f[] plantPos) {
		this.plantPos = plantPos;
	}

	public Point3f[] getGroundPos() {
		return groundPos;
	}

	public void setGroundPos(Point3f[] groundPos) {
		this.groundPos = groundPos;
	}

	public Point3f[] getShelterPos() {
		return shelterPos;
	}

	public void setShelterPos(Point3f[] shelterPos) {
		this.shelterPos = shelterPos;
	}

	public Point3f[] getMovableEnemyPos() {
		return movableEnemyPos;
	}

	public void setMovableEnemyPos(Point3f[] movableEnemyPos) {
		this.movableEnemyPos = movableEnemyPos;
	}

	public Point3f[] getFrozeenEnemyPos() {
		return frozeenEnemyPos;
	}

	public void setFrozeenEnemyPos(Point3f[] frozeenEnemyPos) {
		this.frozeenEnemyPos = frozeenEnemyPos;
	}

	public int getNewBackgroundX() {
		return newBackgroundX;
	}

	public void setNewBackgroundX(int newBackgroundX) {
		this.newBackgroundX = newBackgroundX;
	}

	public void setWaveInterval(long waveInterval) {
		this.waveInterval = waveInterval;
	}

	public long getWaveInterval() {
		return waveInterval;
	}

	public void setBossHealth(int bossHealth) {
		this.bossHealth = bossHealth;
	}

	public int getBossHealth() {
		return bossHealth;
	}
}
