package com.starpxy.map;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.starpxy.util.Point3f;

/**
 * This is class to load the maps from a json file The json file defineds:
 * 
 * 1) The length of the map;
 * 
 * 2) The ground height, length, and position;
 * 
 * 3) The position of NPC refreshes;
 * 
 * 4) The position of treasure boxes.
 * 
 * @author xingyupan
 *
 */
public class MapLoader {
	public static Map loadFromFile(String filename) {
		Map map = null;
		try {
			InputStream inputStream = new FileInputStream(filename);
			String mapString = "";
			String temp;
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			temp = bufferedReader.readLine();
			while (temp != null) {
				mapString += temp;
				temp = bufferedReader.readLine();
			}
			Gson gson = new Gson();
			map = gson.fromJson(mapString, Map.class);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

	public static void writeToFile(Map map, String filename) {
		OutputStream outputStream;
		try {
			outputStream = new FileOutputStream("maps/" + filename + ".json");
			Gson gson = new Gson();
			String mapString = gson.toJson(map);
			outputStream.write(mapString.getBytes());
			outputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		/*
		 * the following codes are just for testing
		 */
		Point3f[] groundPos = new Point3f[39];
		Point3f[] shelterPos = new Point3f[3];
		Point3f[] plantPos = new Point3f[3];
		Point3f[] boxPos = new Point3f[6];
		Point3f[] movableEnemyPos = new Point3f[8];
		Point3f[] frozeenEnemyPos = new Point3f[1];

		groundPos[0] = new Point3f(0, 640, 0);
		groundPos[1] = new Point3f(280, 640, 0);
		groundPos[2] = new Point3f(560, 640, 0);
		groundPos[3] = new Point3f(840, 640, 0);
		groundPos[4] = new Point3f(1120, 640, 0);
		groundPos[5] = new Point3f(1400, 640, 0);
		groundPos[6] = new Point3f(1680, 640, 0);
		groundPos[7] = new Point3f(1680, 540, 0);
		groundPos[8] = new Point3f(2100, 640, 0);
		groundPos[9] = new Point3f(2100, 640, 0);
		groundPos[10] = new Point3f(2100, 640, 0);
		groundPos[11] = new Point3f(2380, 640, 0);
		groundPos[12] = new Point3f(2660, 640, 0);
		groundPos[13] = new Point3f(2940, 640, 0);
		groundPos[14] = new Point3f(3120, 640, 0);
		groundPos[15] = new Point3f(3400, 640, 0);
		groundPos[16] = new Point3f(3400, 300, 0);
		groundPos[17] = new Point3f(3680, 640, 0);
		groundPos[18] = new Point3f(3900, 640, 1);
		groundPos[19] = new Point3f(4100, 640, 1);
		groundPos[20] = new Point3f(4300, 640, 1);
		groundPos[21] = new Point3f(4700, 640, 1);
		groundPos[22] = new Point3f(3960, 250, 1);
		groundPos[23] = new Point3f(4100, 250, 1);
		groundPos[24] = new Point3f(4300, 250, 1);
		groundPos[25] = new Point3f(4500, 250, 1);
		groundPos[26] = new Point3f(4700, 250, 1);
		groundPos[27] = new Point3f(4900, 250, 1);
		groundPos[28] = new Point3f(5100, 250, 1);
		groundPos[29] = new Point3f(3960, 80, 1);
		groundPos[30] = new Point3f(4100, 80, 1);
		groundPos[31] = new Point3f(4300, 80, 1);
		groundPos[32] = new Point3f(4500, 80, 1);
		groundPos[33] = new Point3f(4700, 80, 1);
		groundPos[34] = new Point3f(4900, 80, 1);
		groundPos[35] = new Point3f(5100, 80, 1);
		groundPos[36] = new Point3f(4500, 640, 1);
		groundPos[37] = new Point3f(4900, 640, 1);
		groundPos[38] = new Point3f(5100, 640, 1);

		boxPos[0] = new Point3f(400, 550, 2);
		boxPos[1] = new Point3f(2300, 550, 1);
		boxPos[2] = new Point3f(1600, 440, 3);
		boxPos[3] = new Point3f(3400, 210, 3);
		boxPos[4] = new Point3f(2500, 550, 2);
		boxPos[5] = new Point3f(1200, 550, 1);

		plantPos[0] = new Point3f(480, 460, 1);
		plantPos[1] = new Point3f(1140, 460, 2);
		plantPos[2] = new Point3f(3100, 460, 1);
		shelterPos[0] = new Point3f(720, 520, 0);
		shelterPos[1] = new Point3f(1100, 520, 0);
		shelterPos[2] = new Point3f(2900, 520, 0);

		movableEnemyPos[0] = new Point3f(400, 50, 0);
		movableEnemyPos[1] = new Point3f(800, 50, 0);
		movableEnemyPos[2] = new Point3f(1800, 50, 0);
		movableEnemyPos[3] = new Point3f(1300, 50, 0);
		movableEnemyPos[4] = new Point3f(2000, 50, 0);
		movableEnemyPos[5] = new Point3f(2600, 50, 0);
		movableEnemyPos[6] = new Point3f(3100, 50, 0);
		movableEnemyPos[7] = new Point3f(3800, 50, 0);
		// read out a map
		Map map = new Map(groundPos, shelterPos, plantPos, boxPos, movableEnemyPos, frozeenEnemyPos, 5150, 3850, 300,
				120);
		MapLoader.writeToFile(map, "default-map");
	}
}
