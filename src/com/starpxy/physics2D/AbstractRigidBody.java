package com.starpxy.physics2D;

import com.starpxy.util.Point3f;
import com.starpxy.util.Vector3f;

public abstract class AbstractRigidBody implements Rigidbody {

	protected float mass;
	private boolean xFrozen = false;
	private boolean yFrozen = false;
	private Vector3f velocity = new Vector3f(0, 0, 0);
	private Vector3f angularVelocity = new Vector3f(0, 0, 0);
	private boolean isKinematic = true;
	private boolean useGravity = true;
	private Point3f position;
	private Vector3f rotation;
	private Vector3f acceleration;


	@Override
	public float mass() {
		return mass;
	}

	@Override
	public void freezeX() {
		xFrozen = true;
	}

	@Override
	public void freezeY() {
		yFrozen = true;
	}

	@Override
	public void unfreezeX() {
		xFrozen = false;
	}

	@Override
	public void unfreezeY() {
		yFrozen = false;
	}

	@Override
	public boolean isXFrozen() {
		return xFrozen;
	}

	@Override
	public boolean isYFrozen() {
		return yFrozen;
	}

	@Override
	public void setVelocity(Vector3f velocity) {
		this.velocity = new Vector3f(velocity.getX(), velocity.getY(), velocity.getZ());
	}

	@Override
	public Vector3f getVelocity() {
		return new Vector3f(velocity.getX(), velocity.getY(), velocity.getZ());
	}

	@Override
	public void setGravityStatus(boolean status) {
		useGravity = status;
	}

	@Override
	public boolean getGravityStatus() {
		return useGravity;
	}

	@Override
	public boolean isKinematic() {
		return isKinematic;
	}

	@Override
	public void setKinematicStatus(boolean status) {
		isKinematic = status;
	}

	@Override
	public Point3f position() {
		return new Point3f(position.getX(), position.getY(), position.getZ());
	}

	@Override
	public Vector3f rotation() {
		return new Vector3f(rotation.getX(), rotation.getY(), rotation.getZ());
	}

	@Override
	public void setPosition(Point3f position) {
		this.position = new Point3f(position.getX(), position.getY(), position.getZ());
	}

	@Override
	public void setRotation(Vector3f rotation) {
		this.rotation = new Vector3f(rotation.getX(), rotation.getY(), rotation.getZ());
	}

	@Override
	public Vector3f angularVelocity() {
		return new Vector3f(angularVelocity.getX(), angularVelocity.getY(), angularVelocity.getZ());
	}

	@Override
	public void setAngularVelocity(Vector3f angularVelocity) {
		this.angularVelocity = new Vector3f(angularVelocity.getX(), angularVelocity.getY(), angularVelocity.getZ());
	}

	@Override
	public Vector3f acceleration() {

		return null;
	}
	
	/**
	 * Set the acceleration of the rigidbody, only can be called in the package.
	 * 
	 * @param acc is a vector of the acceleration.
	 */
	protected void setAcceleration(Vector3f acc) {
		acceleration = new Vector3f(acc.getX(), acc.getY(), acc.getZ());
	}

}
