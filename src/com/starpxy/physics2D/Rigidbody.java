package com.starpxy.physics2D;

import com.starpxy.util.Point3f;
import com.starpxy.util.Vector3f;

public interface Rigidbody {

	/**
	 * get the mass of a rigidbody
	 * 
	 * @return a float number of mass
	 */
	public float mass();

	/**
	 * Freeze the movement of X axis
	 */
	public void freezeX();

	/**
	 * Freeze the movement of Y axis
	 */
	public void freezeY();

	/**
	 * unfreeze the movement of X axis
	 */
	public void unfreezeX();

	/**
	 * unfreeze the movement of Y axis
	 */
	public void unfreezeY();

	/**
	 * the X axis frozen status of the rigidbody
	 * 
	 * @return true is frozen; false is unfrozen
	 */
	public boolean isXFrozen();

	/**
	 * the Y axis frozen status of the rigidbody
	 * 
	 * @return true is frozen; false is unfrozen
	 */
	public boolean isYFrozen();

	/**
	 * get the velocity of the rigidbody
	 * 
	 * @return a vector of velocity
	 */
	public Vector3f getVelocity();

	/**
	 * set the velocity of the rigidbody
	 * 
	 * @param velocity is a vector of the velocity
	 */
	public void setVelocity(Vector3f velocity);

	/**
	 * set if use the gravity
	 * 
	 * @param status a boolean value; true is use the gravity
	 */
	public void setGravityStatus(boolean status);

	/**
	 * get the status if use the gravity
	 * 
	 * @return boolean value; true is use the gravity
	 */
	public boolean getGravityStatus();

	/**
	 * if the rigidbody is kinematic
	 * 
	 * @return boolean value; true is kinematic
	 */
	public boolean isKinematic();

	/**
	 * set the kinematic status of the rigidbody
	 * 
	 * @param status boolean value; true is kinematic
	 */
	public void setKinematicStatus(boolean status);

	/**
	 * get the geometric center of the rigidbody
	 * 
	 * @return a point3f object
	 */
	public Point3f position();

	/**
	 * get the rotation vector of the rigidbody
	 * 
	 * @return a vector3f object
	 */
	public Vector3f rotation();

	/**
	 * set the rigidbody position
	 * 
	 * @param position is a point3f object
	 */
	public void setPosition(Point3f position);

	/**
	 * set the rigidbody rotation
	 * 
	 * @param rotation is a vector3f object
	 */
	public void setRotation(Vector3f rotation);

	/**
	 * get the angular velocity of the rigidbody
	 * 
	 * @return a copy of the angularVelocity vector3f
	 */
	public Vector3f angularVelocity();

	/**
	 * set the angular velocity of the rigidbody
	 * 
	 * @param angularVelocity is a vector3f
	 */
	public void setAngularVelocity(Vector3f angularVelocity);

	/**
	 * The acceleration of the rigidbody
	 * 
	 * @return a vector to represent the acceleration
	 */
	public Vector3f acceleration();

	/**
	 * This method provides quickly check if the two rigidbody collide with each
	 * other.
	 * 
	 * @param rigidbody is the other rigidbody that the rect rigidbody to be checked
	 * @return true if it they collide; false if they don't collide
	 */
	public boolean isCollidedWith(Rigidbody rigidbody);
}
