package com.starpxy.physics2D;

import com.starpxy.util.Point3f;
import com.starpxy.util.Vector3f;

/**
 * This is a circle rigidbody class.
 * 
 * @author xingyupan
 *
 */
public class CircleRigidbody extends AbstractRigidBody {
	/**
	 * The redius of the circle
	 */
	private int redius;

	public CircleRigidbody(int redius, float mass, Point3f initPos, Vector3f initRotate) {
		this.redius = redius;
		this.mass = mass;
		setPosition(initPos);
		setRotation(initRotate);
	}

	@Override
	public boolean isCollidedWith(Rigidbody rigidbody) {
		Point3f thisPos = this.position();
		Point3f targetPos = rigidbody.position();
		if (rigidbody instanceof CircleRigidbody) {
			CircleRigidbody circleRigidbody = (CircleRigidbody) rigidbody;
			if (targetPos.distanceOf(thisPos) > (getRedius() + circleRigidbody.getRedius())) {
				return false;
			} else {
				return true;
			}
		} else if (rigidbody instanceof SimpleRectRigidbody) {
			SimpleRectRigidbody simpleRectRigidbody = (SimpleRectRigidbody) rigidbody;
			if (targetPos.getX() >= thisPos.getX()) {
				if (targetPos.getY() >= thisPos.getY()) {
					// Quadrant 1: rect is on right bottom
					Vector3f cToR = targetPos.MinusPoint(thisPos);
					Vector3f rToCo = new Point3f(targetPos.getX() - (simpleRectRigidbody.getRigidWidth() / 2),
							targetPos.getY() - (simpleRectRigidbody.getRigidHeight() / 2), targetPos.getZ())
									.MinusPoint(targetPos);
					Vector3f cToCo = cToR.PlusVector(rToCo);
					if (cToCo.getY() < 0) {
						cToCo.setY(0);
					}
					if (cToCo.getX() < simpleRectRigidbody.getRigidWidth()) {
						cToCo.setX(0);
					}
					if (cToCo.length() <= getRedius()) {
						return true;
					}
				} else {
					// Quadrant 2: rect is on right top
					Vector3f cToR = targetPos.MinusPoint(thisPos);
					Vector3f rToCo = new Point3f(targetPos.getX() - (simpleRectRigidbody.getRigidWidth() / 2),
							targetPos.getY() + (simpleRectRigidbody.getRigidHeight() / 2), targetPos.getZ())
									.MinusPoint(targetPos);
					Vector3f cToCo = cToR.PlusVector(rToCo);
					if (cToCo.getY() > 0) {
						cToCo.setY(0);
					}
					if (cToCo.getX() < simpleRectRigidbody.getRigidWidth()) {
						cToCo.setX(0);
					}
					if (cToCo.length() <= getRedius()) {
						return true;
					}
				}
			} else {
				if (targetPos.getY() >= thisPos.getY()) {
					// Quadrant 3: rect is on left bottom
					Vector3f cToR = targetPos.MinusPoint(thisPos);
					Vector3f rToCo = new Point3f(targetPos.getX() + (simpleRectRigidbody.getRigidWidth() / 2),
							targetPos.getY() - (simpleRectRigidbody.getRigidHeight() / 2), targetPos.getZ())
									.MinusPoint(targetPos);
					Vector3f cToCo = cToR.PlusVector(rToCo);
					if (cToCo.getY() < 0) {
						cToCo.setY(0);
					}
					if (cToCo.getX() < simpleRectRigidbody.getRigidWidth()) {
						cToCo.setX(0);
					}
					if (cToCo.length() <= getRedius()) {
						return true;
					}
				} else {
					// Quadrant 4: rect is on left top
					Vector3f cToR = targetPos.MinusPoint(thisPos);
					Vector3f rToCo = new Point3f(targetPos.getX() + (simpleRectRigidbody.getRigidWidth() / 2),
							targetPos.getY() + (simpleRectRigidbody.getRigidHeight() / 2), targetPos.getZ())
									.MinusPoint(targetPos);
					Vector3f cToCo = cToR.PlusVector(rToCo);
					if (cToCo.getY() > 0) {
						cToCo.setY(0);
					}
					if (cToCo.getX() < simpleRectRigidbody.getRigidWidth()) {
						cToCo.setX(0);
					}
					if (cToCo.length() <= getRedius()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public float getRedius() {
		return redius;
	}

	public void setRedius(int redius) {
		this.redius = redius;
	}

}
