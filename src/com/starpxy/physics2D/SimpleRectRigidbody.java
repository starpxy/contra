package com.starpxy.physics2D;

import com.starpxy.util.Point3f;
import com.starpxy.util.Vector3f;

/**
 * This is a simple Rectangle rigidbody class. All the rectangles are assumed to
 * be rotation-restricted. The rigidbody can be used for detect
 * AABB(Axis-Aligned Bounding Box) collide detection.
 * 
 * @author xingyupan
 *
 */
public class SimpleRectRigidbody extends AbstractRigidBody {
	/**
	 * The width of the rectangle rigidbody (different from the gameobject width)
	 */
	private int rigidWidth;
	/**
	 * The height of the rectangle rigidbody (different from the gameobject height)
	 */
	private int rigidHeight;

	private SimpleRectRigidbody() {

	}

	public SimpleRectRigidbody(int rigidWidth, int rigidHeight, float mass, Point3f initialPos, Vector3f initialRot) {
		this.mass = mass;
		this.rigidHeight = rigidHeight;
		this.rigidWidth = rigidWidth;
		setPosition(initialPos);
		setRotation(initialRot);
		
	}

	@Override
	public boolean isCollidedWith(Rigidbody rigidbody) {
		Point3f thisPos = this.position();
		Point3f targetPos = rigidbody.position();
		if (rigidbody instanceof SimpleRectRigidbody) {
			SimpleRectRigidbody rectRigidbody = (SimpleRectRigidbody) rigidbody;
			if (Math.abs(thisPos.getX() - targetPos.getX()) <= (getRigidWidth() + rectRigidbody.getRigidWidth()) / 2
					&& Math.abs(thisPos.getY()
							- targetPos.getY()) <= (getRigidHeight() + rectRigidbody.getRigidHeight()) / 2) {
				return true;
			} else {
				return false;
			}
		} else if (rigidbody instanceof CircleRigidbody) {
			CircleRigidbody circleRigidbody = (CircleRigidbody) rigidbody;
			if (thisPos.getX() >= targetPos.getX()) {
				if (thisPos.getY() >= targetPos.getY()) {
					// Quadrant 1: rect is on right bottom
					Vector3f cToR = thisPos.MinusPoint(targetPos);
					Vector3f rToCo = new Point3f(thisPos.getX() - (rigidWidth / 2), thisPos.getY() - (rigidHeight / 2),
							thisPos.getZ()).MinusPoint(thisPos);
					Vector3f cToCo = cToR.PlusVector(rToCo);
					if (cToCo.getY() < 0) {
						cToCo.setY(0);
					}
					if (cToCo.getX() < rigidWidth) {
						cToCo.setX(0);
					}
					if (cToCo.length() <= circleRigidbody.getRedius()) {
						return true;
					}
				} else {
					// Quadrant 2: rect is on right top
					Vector3f cToR = thisPos.MinusPoint(targetPos);
					Vector3f rToCo = new Point3f(thisPos.getX() - (rigidWidth / 2), thisPos.getY() + (rigidHeight / 2),
							thisPos.getZ()).MinusPoint(thisPos);
					Vector3f cToCo = cToR.PlusVector(rToCo);
					if (cToCo.getY() > 0) {
						cToCo.setY(0);
					}
					if (cToCo.getX() < rigidWidth) {
						cToCo.setX(0);
					}
					if (cToCo.length() <= circleRigidbody.getRedius()) {
						return true;
					}
				}
			} else {
				if (thisPos.getY() >= targetPos.getY()) {
					// Quadrant 3: rect is on left bottom
					Vector3f cToR = thisPos.MinusPoint(targetPos);
					Vector3f rToCo = new Point3f(thisPos.getX() + (rigidWidth / 2), thisPos.getY() - (rigidHeight / 2),
							thisPos.getZ()).MinusPoint(thisPos);
					Vector3f cToCo = cToR.PlusVector(rToCo);
					if (cToCo.getY() < 0) {
						cToCo.setY(0);
					}
					if (cToCo.getX() < rigidWidth) {
						cToCo.setX(0);
					}
					if (cToCo.length() <= circleRigidbody.getRedius()) {
						return true;
					}
				} else {
					// Quadrant 4: rect is on left top
					Vector3f cToR = thisPos.MinusPoint(targetPos);
					Vector3f rToCo = new Point3f(thisPos.getX() + (rigidWidth / 2), thisPos.getY() + (rigidHeight / 2),
							thisPos.getZ()).MinusPoint(thisPos);
					Vector3f cToCo = cToR.PlusVector(rToCo);
					if (cToCo.getY() > 0) {
						cToCo.setY(0);
					}
					if (cToCo.getX() < rigidWidth) {
						cToCo.setX(0);
					}
					if (cToCo.length() <= circleRigidbody.getRedius()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static void main(String[] args) {
		SimpleRectRigidbody simpleRectRigidbody = new SimpleRectRigidbody(20, 20, 1, new Point3f(100, 100, 0),
				new Vector3f(0, 0, 0));
		CircleRigidbody circleRigidbody = new CircleRigidbody(10, 1, new Point3f(102, 120, 0), new Vector3f(0, 0, 0));
		System.out.println(simpleRectRigidbody.isCollidedWith(circleRigidbody));
	}

	public int getRigidHeight() {
		return rigidHeight;
	}

	public int getRigidWidth() {
		return rigidWidth;
	}

	protected void setRigidHeight(int rigidHeight) {
		this.rigidHeight = rigidHeight;
	}

	protected void setRigidWidth(int rigidWidth) {
		this.rigidWidth = rigidWidth;
	}

}
